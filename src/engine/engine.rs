extern crate ggez;

use crate::civilization::entity::*;

use crate::world::biomes::*;
use crate::world::coordinate::*;
use crate::world::world::*;

use ggez::*;
use ggez::{Context, ContextBuilder, GameResult};
use ggez::event::*;
use ggez::graphics::*;

#[derive(Eq, PartialEq, Debug)]
pub enum SelectionOption<T> {
    Some(T),
    Searching(usize),
    None,
}

pub struct GameEngine {
    pub world: World,
    pub entities: EntityList,

    selected_entity: SelectionOption<String>,

    zoom_level: f32,
    camera_offset: (f32, f32),
}

impl GameEngine {
    pub fn new(world: World, entities: EntityList, _ctx: &mut Context) -> GameEngine {
        // Load/create resources here: images, fonts, sounds, etc.
        return GameEngine {
            world,
            entities,

            selected_entity: SelectionOption::None,

            zoom_level: 1.0,
            camera_offset: (0.0, 0.0),
        }
    }

    pub fn draw_from_world(&mut self, ctx: &mut Context) {
        graphics::clear(ctx, graphics::BLACK);
    
        let window_size = graphics::size(ctx);
        let drawing_center = (window_size.0 / 2.0, window_size.1 / 2.0);
        
        // println!("Window Size: {:?}", window_size);
        // println!("Drawing Center: {:?}", drawing_center);
        // TODO: Figure out why resizing the window makes everything get all stretched out, then remove these prints.
    
        let root3: f32 = 3.0_f32.sqrt();
        let hex_side: f32 = 10.0 * self.zoom_level;
    
        let set = self.world.get_internal_set_clone();
    
        for coordinate in set {
            let color = match self.world.get_tile_at(coordinate).unwrap().get_biome() {
                Biome::Empty => graphics::Color::new(0.1, 0.1, 0.1, 1.0),
                Biome::Tundra => graphics::Color::new(0.7, 0.7, 0.7, 1.0),
                Biome::Taiga => graphics::Color::new(0.45, 0.65, 0.55, 1.0),
    
                Biome::Forest => graphics::Color::new(0.1, 0.45, 0.1, 1.0),
                Biome::Woodland => graphics::Color::new(0.2, 0.55, 0.2, 1.0),
    
                Biome::Chaparral => graphics::Color::new(0.8, 0.8, 0.2, 1.0),
                Biome::Plains => graphics::Color::new(0.6, 0.8, 0.2, 1.0),
                Biome::Savannah => graphics::Color::new(0.85, 0.8, 0.4, 1.0),
    
                Biome::Desert => graphics::Color::new(0.95, 0.75, 0.2, 1.0),
                Biome::Steppes => graphics::Color::new(0.65, 0.6, 0.4, 1.0),
                Biome::CoolDesert => graphics::Color::new(0.75, 0.6, 0.4, 1.0),
    
                Biome::TemperateRainForest => graphics::Color::new(0.05, 0.5, 0.25, 1.0),
                Biome::TropicalForest => graphics::Color::new(0.05, 0.4, 0.25, 1.0),
                Biome::Jungle => graphics::Color::new(0.05, 0.3, 0.25, 1.0),
    
                Biome::TropicalSwamp => graphics::Color::new(0.2, 0.5, 0.4, 1.0),
                Biome::TemperateWetland => graphics::Color::new(0.2, 0.6, 0.35, 1.0),
                Biome::ColdBog => graphics::Color::new(0.3, 0.4, 0.3, 1.0),
    
                Biome::MediterraneanWoods => graphics::Color::new(0.0, 0.6, 0.4, 1.0),
    
                Biome::Badlands => graphics::Color::new(0.6, 0.2, 0.1, 1.0),
    
                Biome::Ocean => graphics::Color::new(0.0, 0.05, 0.3, 1.0),
                Biome::Sea => graphics::Color::new(0.0, 0.1, 0.5, 1.0),
                Biome::Lake => graphics::Color::new(0.0, 0.1, 0.7, 1.0),
                _ => graphics::WHITE,
            };
    
            let hex = graphics::Mesh::new_polygon(
                ctx, 
                graphics::DrawMode::fill(), 
                &[
                    nalgebra::Point2::new(-(root3 * (hex_side/2.0)),  0.5 * hex_side),
                    nalgebra::Point2::new( 0.0                     ,  hex_side),
                    nalgebra::Point2::new( (root3 * (hex_side/2.0)),  hex_side / 2.0),
                    nalgebra::Point2::new( (root3 * (hex_side/2.0)), -hex_side / 2.0),
                    nalgebra::Point2::new( 0.0                     , -hex_side),
                    nalgebra::Point2::new(-(root3 * (hex_side/2.0)), -0.5 * hex_side),
                ], 
                color,
            ).unwrap();
            
            let x = drawing_center.0 + (coordinate.1 as f32 * -((0.5 * hex_side) * root3)) 
                    + (coordinate.2 as f32 * ((0.5 * hex_side) * root3))
                    + (self.camera_offset.0 * self.zoom_level);
            let y = drawing_center.1 + -(coordinate.0 as f32 * (1.5 * hex_side))
                    + (self.camera_offset.1 * self.zoom_level);
            // Negative is placed here ^ because rendering is done from top left not bottom left.
    
            // To help with debugging.
            // if coordinate == (0, 0, 0) {
            //     println!("center tile drawn at {}, {}", x, y);
            //     println!("{:?}", DrawParam::default().dest(nalgebra::Point2::new(x, y)));
            // }
    
            graphics::draw(ctx, &hex, DrawParam::default().dest(nalgebra::Point2::new(x, y),));
    
            let rect_size = 1.0 * hex_side;
            match self.world.get_tile_at(coordinate).unwrap().get_settlement() {
                Some(_) => {
                    let square = graphics::Mesh::new_rectangle(
                        ctx, 
                        graphics::DrawMode::fill(), 
                        graphics::Rect::new(-(0.5*rect_size), -(0.5*rect_size), rect_size, rect_size), 
                        graphics::Color::new(1.0, 0.0, 0.0, 1.0)
                    ).unwrap();
    
                    graphics::draw(ctx, &square, (nalgebra::Point2::new(x, y),));
                },
                None => {}
            }
        }

        for entity in self.entities.get_entities() {
            let entity_size = 1.00 * hex_side;

            let tri = graphics::Mesh::new_polygon(
                ctx, 
                graphics::DrawMode::fill(), 
                &[
                    nalgebra::Point2::new(-(0.5 * entity_size), -0.25 * (root3 * entity_size)),
                    nalgebra::Point2::new( (0.5 * entity_size), -0.25 * (root3 * entity_size)),
                    nalgebra::Point2::new( 0.0                ,  0.25 * (root3 * entity_size)),
                ], 
                graphics::Color::new(0.0, 1.0, 1.0, 1.0),
            ).unwrap();

            let entity_location = entity.location().as_tuple();

            let entity_x = drawing_center.0 + (entity_location.1 as f32 * -((0.5 * hex_side) * root3)) 
                    + (entity_location.2 as f32 * ((0.5 * hex_side) * root3))
                    + (self.camera_offset.0 * self.zoom_level);
            let entity_y = drawing_center.1 + -(entity_location.0 as f32 * (1.5 * hex_side))
                    + (self.camera_offset.1 * self.zoom_level);

            graphics::draw(ctx, &tri, (nalgebra::Point2::new(entity_x, entity_y),));
        }

        match self.check_tile_mouseover(ctx, drawing_center, hex_side, root3) {
            Some(hex_coordinate) => {
                self.highlight_tile_at(hex_coordinate.clone(), ctx, drawing_center, hex_side, root3);
                self.display_tile_info_as_text(hex_coordinate.clone(), ctx, drawing_center, hex_side, root3);

                match self.selected_entity {
                    SelectionOption::Searching(number) => {
                        self.selected_entity = SelectionOption::Some(
                            self.entities.get_aliases_at(hex_coordinate)[number - 1].clone()
                        );
                    }
                    _ => {}
                }
            },
            None => {
                self.display_help_text(ctx);
            }
        }

        self.display_selected_entity_alias(ctx);
    
        graphics::present(ctx);
    }

    fn check_tile_mouseover(&self, ctx: &mut Context, drawing_center: (f32, f32), hex_side: f32, root3: f32) -> Option<Coordinate> {
        let mouse_position = ggez::input::mouse::position(ctx);

        let adjusted_x = mouse_position.x - drawing_center.0 - (self.camera_offset.0 * self.zoom_level);
        let adjusted_y = mouse_position.y - drawing_center.1 - (self.camera_offset.1 * self.zoom_level);

        let mut ex = -adjusted_y / (1.5 * hex_side);
        // println!("Estimated mouse X: {}", ex);
        if ex > 0.6 {
            ex += 0.5;
        } else if ex < -0.6 {
            ex -= 0.5;
        }

        let mut ey = -(adjusted_x / (root3 * hex_side)) - (-adjusted_y / (3.0 * hex_side));
        // println!("Estimated mouse Y: {}", ey);
        if ey > 0.6 {
            ey += 0.5;
        } else if ey < -0.6 {
            ey -= 0.5;
        }

        let mut ez = (adjusted_x / (root3 * hex_side)) - (-adjusted_y / (3.0 * hex_side));
        // println!("Estimated mouse Z: {}", ez);
        if ez > 0.6 {
            ez += 0.5;
        } else if ez < -0.6 {
            ez -= 0.5;
        }

        // println!("Estimated hex coordinate: ({:.1}, {:.1}, {:.1}).", ex, ey, ez);
        // println!("Estimated hex coordinate: ({}, {}, {}).", ex.trunc(), ey.trunc(), ez.trunc());

        if (ex.trunc() + ey.trunc() + ez.trunc()) == 0.0 {
            return Some(Coordinate::from_tuple((ex.trunc() as i32, ey.trunc() as i32, ez.trunc() as i32)));
        } else {
            return None;
        }

        // if fx and fy are the floating point coordinates on the canvas and (x, y, z) are the hexagon coordinates, then:
        // this goes (y, z) => fx
        //   fx = cx + (0.5 * s * r3) * ((-y) + (z)) + (xo * zl)
        // i need to create fx => (y, z)
        //   (fx - cx - (xo * zl)) / (0.5 * s * r3) = ((-y) + (z))
        
        // this goes (x) => fy
        //   fy = cy + -(x * 1.5 * s) + (yo * zl)
        // i need to create fy => (x)
        //   -1 * [fy - cy - (yo * zl)] / [-1.5 * -s] = x
    }

    fn highlight_tile_at(&self, coordinate: Coordinate, ctx: &mut Context, drawing_center: (f32, f32), hex_side: f32, root3: f32){
        let highlight_size = 1.2;

        let hex = graphics::Mesh::new_polygon(
            ctx, 
            graphics::DrawMode::fill(), 
            &[
                nalgebra::Point2::new(-(root3 * (hex_side/2.0)) * highlight_size,   0.5 * hex_side  * highlight_size),
                nalgebra::Point2::new( 0.0                      * highlight_size,   hex_side        * highlight_size),
                nalgebra::Point2::new( (root3 * (hex_side/2.0)) * highlight_size, ( hex_side / 2.0) * highlight_size),
                nalgebra::Point2::new( (root3 * (hex_side/2.0)) * highlight_size, (-hex_side / 2.0) * highlight_size),
                nalgebra::Point2::new( 0.0                      * highlight_size,  -hex_side        * highlight_size),
                nalgebra::Point2::new(-(root3 * (hex_side/2.0)) * highlight_size,  -0.5 * hex_side  * highlight_size),
            ], 
            graphics::Color::new(1.0, 1.0, 1.0, 0.2),
        ).unwrap();
        
        // This math calculates a location on the screen from a given (x, y, z) hex-coordinate.
        let x = drawing_center.0 + (coordinate.y() as f32 * -((0.5 * hex_side) * root3)) 
                + (coordinate.z() as f32 * ((0.5 * hex_side) * root3))
                + (self.camera_offset.0 * self.zoom_level);
        let y = drawing_center.1 + -(coordinate.x() as f32 * (1.5 * hex_side))
                + (self.camera_offset.1 * self.zoom_level);
        // Negative is placed here ^ because rendering is done from top left not bottom left.
        
        graphics::draw(ctx, &hex, (nalgebra::Point2::new(x, y),));
    }

    fn display_tile_info_as_text(&self, coordinate: Coordinate, ctx: &mut Context, drawing_center: (f32, f32), hex_side: f32, root3: f32){
        match self.world.get_tile_at(coordinate.as_tuple()) {
            Some(tile_ref) => {
                let mut tile_text = Text::new(format!("Location: {:?}. \n", tile_ref.get_coordinates()));
                tile_text.add(format!("Biome: {:?}. \n", tile_ref.get_biome()));
                tile_text.add(format!("Features: {:?}. \n", tile_ref.get_biome_features()));
                tile_text.add(format!("   The region has {:.4}% available land. \n", tile_ref.get_biome_characteristics().available_land()));
                tile_text.add(format!("   The region has {:.4}% forest coverage. \n", tile_ref.get_biome_characteristics().forest_coverage()));
                tile_text.add(format!("   The region has {:.4}% arable land coverage. \n", tile_ref.get_biome_characteristics().arable_land_coverage()));
                tile_text.add(format!("   The region has {:.4}% fertility rating. \n", tile_ref.get_biome_characteristics().fertility_rating()*100.0));
                tile_text.add(format!("   The region is located at an altitude of {:.4}. \n", tile_ref.get_biome_characteristics().altitude()));
                tile_text.add(format!("   The region has an average temperature of {:.4}. \n", tile_ref.get_biome_characteristics().temperature()));

                if self.entities.get_aliases_at(coordinate.clone()).len() > 0 {
                    tile_text.add(format!("There are entities here with aliases {:?}. \n", self.entities.get_aliases_at(coordinate.clone())));
                    tile_text.add(format!("   Select the 1st entity with the '1' key. \n"));
                }

                match tile_ref.get_settlement() {
                    Some(settlement) => {
                        tile_text.add(format!("There is a settlement here named {:?}. \n", settlement.name()));
                        tile_text.add(format!("   The settlement's contains the following buildings: \n"));
                        if settlement.name() == String::from("Lestallum") {
                            // Hardcoded for this version of the demo!
                            tile_text.add(format!("      20 Homesteads \n"));
                            tile_text.add(format!("      10 Woodcutters \n"));
                            tile_text.add(format!("      5 Gristmills \n"));
                        }
                        if settlement.name() == String::from("Crown") {
                            // Hardcoded for this version of the demo!
                            tile_text.add(format!("      4 Farmlands \n"));
                            tile_text.add(format!("      1 Library \n"));
                        }
                        tile_text.add(format!("   The settlement's stockpile contains the following: \n"));
                        for entry in settlement.get_stockpile().get_storage() {
                            tile_text.add(format!("      {:?}: {} \n", entry.0, entry.1));
                        }
                        tile_text.add(format!("Press Spacebar to progress the Settlement by one day, producing daily resources. \n"));
                        tile_text.add(format!("   Please note, during the first few days, the settlement will be building. Be patient! \n"));
                    }
                    None => {}
                };
                graphics::queue_text(ctx, &tile_text, nalgebra::Point2::new(0.0, 0.0), Some(graphics::WHITE));

                graphics::draw_queued_text(ctx, DrawParam::default(), None, FilterMode::Linear);
            },
            None => {
                self.display_help_text(ctx);
            }
        };
    }

    fn display_help_text(&self, ctx: &mut Context) {
        let mut help_text = Text::new("Mouse over a tile to reveal information about it. \n");
        help_text.add(format!("   The red square represents a Settlement, where people live and work. \n"));
        help_text.add(format!("   The teal triange represents an Entity, an individual or group of explorers. \n"));
        help_text.add(format!("   Try mousing over their tiles to find out more! \n"));
        help_text.add(format!("   If your world generated without these symbols, try relaunching the game! \n"));
        help_text.add(format!("Move the camera with arrow keys. \n"));
        help_text.add(format!("Zoom in and out with PageUp and PageDown."));

        graphics::queue_text(ctx, &help_text, nalgebra::Point2::new(0.0, 0.0), Some(graphics::Color::new(0.9, 0.9, 0.9, 1.0)));

        graphics::draw_queued_text(ctx, DrawParam::default(), None, FilterMode::Linear);
    }

    fn display_selected_entity_alias(&self, ctx: &mut Context) {
        let mut entity_text = Text::new("Selected Entity: ");
        match &self.selected_entity {
            SelectionOption::Some(alias) => {
                entity_text.add(format!("{:?}. \n", alias));
                entity_text.add(format!("Press 0 to deselect the entity. \n"));
                entity_text.add(format!("Move the entity using W E A D Z X. \n"));
                entity_text.add(format!("These buttons correspond to the six hexagonal directions."));
                graphics::queue_text(ctx, &entity_text, nalgebra::Point2::new(0.0, graphics::size(ctx).1 - 95.0), Some(graphics::Color::new(0.1, 0.9, 0.5, 1.0)));
            }
            SelectionOption::Searching(_) => {
                entity_text.add(format!("None."));
                graphics::queue_text(ctx, &entity_text, nalgebra::Point2::new(0.0, graphics::size(ctx).1 - 45.0), Some(graphics::Color::new(0.1, 0.9, 0.5, 1.0)));
            }
            SelectionOption::None => {
                entity_text.add(format!("None."));
                graphics::queue_text(ctx, &entity_text, nalgebra::Point2::new(0.0, graphics::size(ctx).1 - 45.0), Some(graphics::Color::new(0.1, 0.9, 0.5, 1.0)));
            }
        }

        graphics::draw_queued_text(ctx, DrawParam::default(), None, FilterMode::Linear);
    }
}

impl EventHandler for GameEngine {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        // Update code here...

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx, graphics::WHITE);

        // Draw code here...
        self.draw_from_world(ctx);

        timer::yield_now();

        Ok(())
    }

    fn key_down_event(&mut self, ctx: &mut Context, keycode: KeyCode, _keymods: KeyMods, _repeat: bool) {
        match keycode {
            KeyCode::PageUp =>   { self.zoom_level *= 2.0 },
            KeyCode::PageDown => { self.zoom_level *= 0.5 },

            KeyCode::Up =>    { self.camera_offset.1 +=  100.0 },
            KeyCode::Down =>  { self.camera_offset.1 += -100.0 },
            KeyCode::Left =>  { self.camera_offset.0 +=  100.0 },
            KeyCode::Right => { self.camera_offset.0 += -100.0 },

            KeyCode::Space => {
                for position in self.world.get_internal_set_clone() {
                    match self.world.get_mut_tile_at(position) {
                        Some(tile) => {
                            match tile.get_mut_settlement() {
                                Some(settlement) => {  
                                    settlement.refresh_job_list();
                                    settlement.distribute_labor();
                                    settlement.produce_from_labor_manifest();
                                    settlement.consume_food();
                                },
                                None => {},
                            }
                        },
                        None => {}
                    }
                }
            }

            KeyCode::Key0 => { self.selected_entity = SelectionOption::None }
            KeyCode::Key1 => { self.selected_entity = SelectionOption::Searching(1) }

            KeyCode::W => {
                match &self.selected_entity {
                    SelectionOption::Some(alias) => {
                        self.entities.move_aliased_entity(&mut self.world, alias.clone(), Direction::Northwest);
                    },
                    _ => {}
                }
            }
            KeyCode::E => {
                match &self.selected_entity {
                    SelectionOption::Some(alias) => {
                        self.entities.move_aliased_entity(&mut self.world, alias.clone(), Direction::Northeast);
                    },
                    _ => {}
                }
            }
            KeyCode::A => {
                match &self.selected_entity {
                    SelectionOption::Some(alias) => {
                        self.entities.move_aliased_entity(&mut self.world, alias.clone(), Direction::West);
                    },
                    _ => {}
                }
            }
            KeyCode::D => {
                match &self.selected_entity {
                    SelectionOption::Some(alias) => {
                        self.entities.move_aliased_entity(&mut self.world, alias.clone(), Direction::East);
                    },
                    _ => {}
                }
            }
            KeyCode::Z => {
                match &self.selected_entity {
                    SelectionOption::Some(alias) => {
                        self.entities.move_aliased_entity(&mut self.world, alias.clone(), Direction::Southwest);
                    },
                    _ => {}
                }
            }
            KeyCode::X => {
                match &self.selected_entity {
                    SelectionOption::Some(alias) => {
                        self.entities.move_aliased_entity(&mut self.world, alias.clone(), Direction::Southeast);
                    },
                    _ => {}
                }
            }
            _ => {}
        }
    }
}