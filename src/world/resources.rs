use std::collections::*;

//              --- R E S O U R C E S ---
// The basic building block of the resource system. Defines the different types of resources.
#[derive(Eq, PartialEq, Clone, Hash, Debug)]
pub enum Resource {
    Generic,        // A generic resource that can act as a sort of null value.
    Thing(String),  // A resource that can be given any string to become a custom resource. Special use only.

    // WOOD
    Wood,
    HardWood,
    Lumber,
    HardLumber,
    Charcoal,

    // BASIC METALS
    CopperOre,
    Copper,
    TinOre,
    Tin,
    IronOre,
    Iron,
    LeadOre,
    Lead,

    // ADVANCED METALS
    AluminumOre,
    Aluminum,
    ZincOre,
    Zinc,
    NickelOre,
    Nickel,

    // PRECIOUS METALS
    GoldOre,
    Gold,
    SilverOre,
    Silver,

    // ALLOYS
    Steel,
    Bronze,
    Brass,

    // COMPOUNDS
    Coal,
    Clay,
    Rubber,
    Glass,

    // STONE
    Stone, //can be cut from any stone
    Slate,
    Marble,
    Granite,
    Sandstone,
    Limestone,

    // INDUSTRIAL CROPS
    Cotton,
    Flax,
    Hemp,

    // FOOD CROPS
    // Might go into more detail with crops or add a few "winter crops" for cold regions.
    Grains(GrainType),
        // Tropical: Millet, Soybean, Corn
        // Temperate: Rice, Oats, Beans
        // Cold: Wheat, Barley, Lentil
        // Straw is a byproduct!!!
    Berries(BerryType),
        // Tropical: Grape
        // Temperate: Blueberry, Blackberry, Raspberry, Strawberry, [Wildberry]
        // Cold: Cranberry
    Fruits(FruitType),
        // Tropical: Banana, Coconut, Pineapple, Lemon, Orange, (Fig), (Durian), (Jackfruit)
        // Temperate: Pear, Melon, (Lime), Lychee, Pomegranate
        // Cold: Peach, Apple, Cherry
    Vegetables(VegetableType),
        // Tropical: Nuts, Tomato, Cucumber
        // Temperate: Lettuce, Kale, Potato, Yams, Celery, Garlic, Onions
        // Cold: Cabbage, Carrots, Brocoli, Turnips, Daikon, Sweet Potato, Pumpkin
    Spices(SpiceType),
    ArtisanPlants(ArtisanPlantType),
        // Tropical: Coffee, Cocoa
        // Temperate: Tea
        // Cold: Hops

    // Food products.
    Straw,
    Flour,
    

    // LIVESTOCK
    Sheep,
    Cows,
    Pigs,
    Chickens,

    Goats,
    Alpacas,

    Horses,

    WildGame, // not livestock, but y'know

    // ANIMAL PRODUCTS
    Meat,
    Eggs,
    Wool,
    Milk,
    Leather,

    // OCEANS
    Fish,
    WhaleOil,

    // SPECIAL
    FreshWater,

    // WASTE
    IndustrialWaste,
    ConsumerWaste,
    HumanWaste,
    AnimalWaste,

    // POWER
    MechanicalPower, // THIS RESOURCE SHOULD NEVER BE STORED!!!
    
    // MANA
    Mana, // maybe split into several types

    // CULTURE RESOURCES
    Unity,
    Influence,

    // RESEARCH RESOURCES
    ResearchGeneric,
}

#[derive(Eq, PartialEq, Clone, Hash, Debug)]
pub enum GrainType {
    Generic,    // generic grain type
    Soybean,    // warm
    Corn,       // warm
    Rice,       // temperate, wet (needs infrastructure)
    Beans,      // temperate
    Oats,       // temperate or cold
    Wheat,      // cold
    Barley,     // cold
}

#[derive(Eq, PartialEq, Clone, Hash, Debug)]
pub enum BerryType {
    Generic,    // generic berry type
    Grapes,     // warm
    Wildberry,  // temperate
    Cranberry,  // cold, wet (needs infrastructure)
}

#[derive(Eq, PartialEq, Clone, Hash, Debug)]
pub enum FruitType {
    Generic,    // generic fruit type
}

#[derive(Eq, PartialEq, Clone, Hash, Debug)]
pub enum VegetableType {
    Generic,
}

#[derive(Eq, PartialEq, Clone, Hash, Debug)]
pub enum SpiceType {
    Generic,
}

#[derive(Eq, PartialEq, Clone, Hash, Debug)]
pub enum ArtisanPlantType {
    Generic,
    CoffeeBean, // warm
    CocoaBean,  // warm
    Tea,        // temperate
    Hops,       // cold
}

// Hash function removed because Hash is now derived for Resource.

// Deposits represent places that resources come from.
// Note: Deposits are a work in progress and will not express where ALL resources come from.
// For example, most renewable resources do not come from deposits.
#[derive(Clone)]
pub struct Deposit {
    resource: Resource,

    base_yield: f64,
    sustainability: f64,
    amount: f64,
    initial_amount: f64,
}

impl Deposit {
    // Creates a new deposit with the specified parameters.
    pub fn new(resource: Resource, base_yield: f64, sustainability: f64, amount: f64) -> Deposit {
        let new_deposit = Deposit {
            resource,
            base_yield,
            sustainability,
            amount,
            initial_amount: amount,
        };
        return new_deposit;
    }

    // pub fn produce(&mut self) -> Bundle {
    //     if self.infinite || self.amount >= self.output {
    //         let b = Bundle::new(self.resource.clone(), self.output);
    //         self.amount -= self.output;

    //         return b;
    //     } else if self.amount > 0 {
    //         let b = Bundle::new(self.resource.clone(), self.amount);
    //         self.amount = 0;

    //         return b;
    //     } else {
    //         let b = Bundle::new(self.resource.clone(), 0);

    //         return b;
    //     }
    // }
}


//          --- B U N D L E S   A N D   S T O C K P I L E S ---
// Bundles represent specific amounts of individual resources. They can be thought of as the basic unit of all resources.
#[derive(Clone)]
pub struct Bundle {
    resource: Resource,
    amount: f64,
}

impl Bundle {
    // Creates a new bundle of the specified resource and amount.
    pub fn new(resource: Resource, amount: f64) -> Bundle {
        let new_bundle = Bundle {
            resource,
            amount,
        };
        return new_bundle;
    }

    // Accessor method. Returns the type of resource in the bundle.
    pub fn resource(&self) -> Resource {
        return self.resource.clone();
    }

    // Accessor method. Returns the amount of resource in the bundle.
    pub fn amount(&self) -> f64 {
        return self.amount;
    }
}

#[derive(Clone, Debug)]
pub struct Stockpile {
    storage: HashMap<Resource, f64>,
    whitelist: Option<Vec<Resource>>,
    // capacity
}

impl Stockpile {
    pub fn new() -> Stockpile {
        return Stockpile {
            storage: HashMap::new(),
            whitelist: None,
        };
    }

    pub fn insert_raw(&mut self, resource: Resource, amount: f64) -> Option<Bundle> {
        match &self.whitelist {
            Some(vector) => {   // if the stockpile has a whitelist
                if vector.contains(&resource) { // if the resource is whitelisted
                    if self.storage.contains_key(&resource) {
                        let current = self.storage.get(&resource).unwrap_or(&0.0);
                        let new_amount = current + amount;
                        self.storage.insert(resource, new_amount);
                        return None;
                    } else {
                        self.storage.insert(resource, amount);
                        return None;
                    }
                } else {    // if the resource isn't whitelisted
                    return Some(Bundle::new(resource, amount));
                }
            },
            None => {   // if the stockpile doesn't have a whitelist and accepts anything
                if self.storage.contains_key(&resource) {
                    let current = self.storage.get(&resource).unwrap_or(&0.0);
                    let new_amount = current + amount;
                    self.storage.insert(resource, new_amount);
                    return None;
                } else {
                    self.storage.insert(resource, amount);
                    return None;
                }
            }
        }
    }

    pub fn insert(&mut self, bundle: Bundle) -> Option<Bundle> {
        match &self.whitelist {
            Some(vector) => {
                if vector.contains(&bundle.resource()) {
                    if self.storage.contains_key(&bundle.resource()) {
                        let current = self.storage.get(&bundle.resource()).unwrap_or(&0.0);
                        let new_amount = current + bundle.amount();
                        self.storage.insert(bundle.resource(), new_amount);
                        return None;
                    } else {
                        self.storage.insert(bundle.resource(), bundle.amount());
                        return None;
                    }
                } else {
                    return Some(bundle);
                }
            },
            None => {
                if self.storage.contains_key(&bundle.resource()) {
                    let current = self.storage.get(&bundle.resource()).unwrap_or(&0.0);
                    let new_amount = current + bundle.amount();
                    self.storage.insert(bundle.resource(), new_amount);
                    return None;
                } else {
                    self.storage.insert(bundle.resource(), bundle.amount());
                    return None;
                }
            }
        }
    }

    // TODO: both insert functions need to be able to handle inserting into stockpiles that are
    // already at their maximum capacity.

    pub fn withdraw(&mut self, resource: Resource, amount: f64) -> Option<Bundle> {
        let stored = *self.storage.get(&resource).unwrap_or(&0.0);
        let mut withdrawal = 0.0;
        let mut new_amount = 0.0;

        if stored > amount {
            new_amount = stored - amount;
            withdrawal = amount;
        } else {
            new_amount = 0.0;
            withdrawal = stored;
        }

        self.storage.insert(resource.clone(), new_amount);
        return Some(Bundle::new(resource.clone(), withdrawal));
    }

    pub fn as_neat_string(&self) -> String {
        let iter = self.storage.iter();

        let mut collection = String::from("");
        for entry in iter {
            collection.push_str(&format!("{:?}: {}", entry.0, entry.1));
            collection.push('\u{000A}');
        }
        return collection;
    }

    pub fn get_storage(&self) -> &HashMap<Resource, f64> {
        return &self.storage;
    }
}


//          --- P R O D U C T I O N ---
// ProductionRating is a representation of how effectively resources and goods can be produced.
#[derive(PartialEq, Clone, Copy)]
pub struct ProductionRating {
    value: f64,
    quality: ProductionQuality,
}

impl ProductionRating {
    // Creates a new ProductionRating with a Value and Quality.
    fn new(value: f64, quality: ProductionQuality) -> ProductionRating {
        let new_production_rating = ProductionRating {
            value,
            quality,
        };
        return new_production_rating;
    }

    // Creates a new ProductionRating with a 0.0 Value and an Empty Quality.
    fn empty() -> ProductionRating {
        let new_production_rating = ProductionRating {
            value: 0.0,
            quality: ProductionQuality::Empty,
        };
        return new_production_rating;
    }

    // Accessor method.
    fn value(&self) -> f64 {
        return self.value;
    }

    // Accessor method.
    fn quality(&self) -> ProductionQuality {
        return self.quality.clone();
    }

    // Mutator method.
    fn write_value(&mut self, new_value: f64) -> Option<f64> {
        let old_value = self.value;
        self.value = new_value;
        return Some(old_value);
    }

    // Mutator method.
    fn write_quality(&mut self, new_quality: ProductionQuality) -> Option<ProductionQuality> {
        let old_quality = self.quality;
        self.quality = new_quality;
        return Some(old_quality);
    }
}

// ProductionQuality can be thought of as one a modifier on the productivity.
#[derive(PartialEq, Clone, Copy)]
pub enum ProductionQuality {
    Empty,
    Awful,
    Poor,
    Fair,
    Average,
    Good,
    Excellent,
    Premium,
}

// ProductionType is an enum of the different types of production.
#[derive(Eq, PartialEq, Clone, Hash)]
pub enum ProductionType {
    LightIndustry,  // Mostly produces consumer goods.
    HeavyIndustry,  // Mostly produces military goods.

    Agriculture,    // Produces crops.
    Herding,        // Produces livestock and animal products.

    Mining,         // Produces raw materials (primarily ores and compounds) from mines.
    Quarrying,      // Produces stone from quarries.
    Forestry,       // Produces wood from forests.

    Processing,     // Converts resources into other resources (ore into metal, wood into lumber, etc).
    Extraction,     // Extracts resources from terrain features or other resources.

    Science,        // Researches new technologies.
    Culture,        // Produces cultural works, art, music, etc.
    Faith,          // Effects to be determined.
}

// ProductivitySet tracks collections of ProductionRatings.
#[derive(PartialEq, Clone)]
pub struct ProductivitySet {
    ratings: HashMap<ProductionType, ProductionRating>,
}

impl ProductivitySet {
    pub fn new() -> ProductivitySet {
        let mut produc = ProductivitySet {
            ratings: HashMap::with_capacity(12), //number of Production variants
        };
        produc.ratings.insert(ProductionType::LightIndustry, ProductionRating::empty());
        produc.ratings.insert(ProductionType::HeavyIndustry, ProductionRating::empty());
        produc.ratings.insert(ProductionType::Agriculture, ProductionRating::empty());
        produc.ratings.insert(ProductionType::Herding, ProductionRating::empty());
        produc.ratings.insert(ProductionType::Mining, ProductionRating::empty());
        produc.ratings.insert(ProductionType::Quarrying, ProductionRating::empty());
        produc.ratings.insert(ProductionType::Forestry, ProductionRating::empty());
        produc.ratings.insert(ProductionType::Processing, ProductionRating::empty());
        produc.ratings.insert(ProductionType::Extraction, ProductionRating::empty());
        produc.ratings.insert(ProductionType::Science, ProductionRating::empty());
        produc.ratings.insert(ProductionType::Culture, ProductionRating::empty());
        produc.ratings.insert(ProductionType::Faith, ProductionRating::empty());
        return produc;
    }

    pub fn overwrite_value(&mut self, t: ProductionType, v: f64) {
        self.ratings.get_mut(&t).unwrap().write_value(v);
    }

    pub fn get_value(&mut self, t: ProductionType) -> f64 {
        return self.ratings.get(&t).unwrap().value();
    }

    pub fn overwrite_quality(&mut self, t: ProductionType, q: ProductionQuality) {
        self.ratings.get_mut(&t).unwrap().write_quality(q);
    }

    pub fn get_quality(&mut self, t: ProductionType) -> ProductionQuality {
        return self.ratings.get(&t).unwrap().quality();
    }
}

pub struct ProductionBundle {
    production: ProductionType,
    capacity: f64,
}