extern crate rand;

use crate::civilization::development::*;
use crate::civilization::settlement::*;
use crate::civilization::unit::*;

use crate::world::biomes::*;
use crate::world::coordinate::*;
use crate::world::resources::*;

use rand::*;

pub const TILE_SIZE: f64 = 100.00;

// Tiles are discrete areas on the world map.
pub struct Tile {
    location: Coordinate,

    biome: Biome,
    features: Vec<BiomeFeature>,
    deposits: Vec<Deposit>,
    productivity: ProductivitySet,
    characteristics: TileCharacteristics,

    settlement: Option<Settlement>,
    developments: Vec<TileDevelopment>,

    // units: Vec<Unit>,
}

impl Tile {
    // Creates a new tile with the provided coordinates and biome.
    pub fn new(x: i32, y: i32, z: i32, biome: Biome) -> Tile {
        let t = Tile {
            location: Coordinate::new(x, y, z),

            biome: biome,
            features: Vec::new(),
            deposits: Vec::new(),
            productivity: ProductivitySet::new(),
            characteristics: TileCharacteristics::default(),

            settlement: None,
            developments: Vec::new(),
            
            // units: Vec::new(),
        };
        return t;
    }

    // Measures the distance between the tile and the given coordinates and returns an integer.
    pub fn distance_to(&self, x: i32, y: i32, z: i32) -> i32 {
        let mut x_diff = self.location.x() - x;
        let mut y_diff = self.location.y() - y;
        let mut z_diff = self.location.z() - z;
        x_diff = x_diff.abs();
        y_diff = y_diff.abs();
        z_diff = z_diff.abs();
        let sum = x_diff + y_diff + z_diff;
        if sum % 2 != 0 {
            println!("ERROR! Sum is not an even number, so something is wrong with the coordinates.")
        }
        return sum / 2;
    }

    // Returns a boolean of whether or not the given coordinate is adjacent.
    pub fn is_adjacent_to(&self, x: i32, y: i32, z: i32) -> bool {
        let d = self.distance_to(x, y, z);
        if d == 1 {
            return true;
        } else {
            return false;
        }
    }

    // Returns a tuple of the tile's coordinates.
    pub fn get_coordinates(&self) -> (i32, i32, i32){
        return self.location.as_tuple();
    }

    // Accessor method.
    pub fn get_biome(&self) -> Biome {
        return self.biome.clone();
    }

    // Accessor method. Returns a vector of the tile's BiomeFeatures.
    pub fn get_biome_features(&self) -> Vec<BiomeFeature> {
        return self.features.clone();
    }

    pub fn get_biome_characteristics(&self) -> TileCharacteristics {
        return self.characteristics.clone();
    }

    // Mutator method.
    pub fn change_biome(&mut self, b: Biome) {
        self.biome = b.clone();
    }

    // Mutator method.
    pub fn add_biome_feature(&mut self, f: BiomeFeature) {
        self.features.push(f);
    }
    
    // Mutator method.
    pub fn add_deposit(&mut self, d: Deposit) {
        self.deposits.push(d);
    }

    // Mutator method.
    pub fn set_production_value(&mut self, t: ProductionType, v: f64){
        self.productivity.overwrite_value(t, v);
    }

    // Mutator method.
    pub fn set_production_quality(&mut self, t: ProductionType, q: ProductionQuality){
        self.productivity.overwrite_quality(t, q);
    }

    // Mutator method.
    pub fn modify_production_value(&mut self, t: ProductionType, change: f64){
        let old_value = self.productivity.get_value(t.clone());
        let new_value = old_value + change;
        self.productivity.overwrite_value(t, new_value);
    }

    // Mutator method.
    pub fn add_settlement(&mut self, new_settlement: Settlement){
        self.settlement = Some(new_settlement);
    }

    pub fn get_settlement(&self) -> Option<&Settlement> {
        return self.settlement.as_ref();
    }

    pub fn get_mut_settlement(&mut self) -> Option<&mut Settlement> {
        return self.settlement.as_mut();
    }

    // Accessor method. Returns a mutable reference to the tile's TileCharacteristics struct.
    pub fn get_mut_characteristics(&mut self) -> &mut TileCharacteristics {
        return &mut self.characteristics;
    }

    // Builds a tile's characteristics based on its biome and features.
    pub fn build_characteristics(&mut self) {
        let mut rng = thread_rng();

        match self.biome {
            Biome::Tundra => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(0.00, 30.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(10.00, 60.00));
                self.characteristics.modify_fertility_rating(0.50);
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-30, -10));
            },
            Biome::Taiga => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(70.00, 100.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(10.00, 80.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.45, 0.65));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-20, -5));
            },
            Biome::Forest => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(70.00, 100.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(40.00, 100.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.75, 1.05));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-10, 10));
            },
            Biome::Woodland => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(50.00, 90.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(40.00, 100.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.85, 1.15));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-10, 10));
            },
            Biome::Chaparral => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(30.00, 90.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(30.00, 90.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.75, 1.05));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-5, 15));
            },
            Biome::Plains => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(0.00, 30.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(50.00, 100.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.85, 1.15));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-5, 15));
            },
            Biome::Savannah => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(0.00, 10.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(40.00, 80.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.60, 1.10));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(5, 25));
            },
            Biome::Desert => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(0.00, 2.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(5.00, 30.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.05, 0.30));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(10, 30));
            },
            Biome::Steppes => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(0.00, 10.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(20.00, 80.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.25, 0.75));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-20, 15));
            },
            Biome::CoolDesert => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(0.00, 2.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(5.00, 30.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.05, 0.30));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-10, 10));
            },
            Biome::TemperateRainForest => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(80.00, 100.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(40.00, 90.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.80, 1.10));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(0, 20));
            },
            Biome::TropicalForest => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(90.00, 100.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(40.00, 80.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.85, 1.15));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(10, 30));
                
            },
            Biome::Jungle => {
                self.characteristics.modify_available_land(-10.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(90.00, 100.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(20.00, 70.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.70, 1.00));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(10, 30));
            },
            Biome::TropicalSwamp => {
                self.characteristics.modify_available_land(rng.gen_range(-20.00, -5.00));
                self.characteristics.modify_forest_coverage(rng.gen_range(40.00, 80.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(10.00, 60.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(1.05, 1.35));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(0, 20));
            },
            Biome::TemperateWetland => {
                self.characteristics.modify_available_land(rng.gen_range(-15.00, 0.00));
                self.characteristics.modify_forest_coverage(rng.gen_range(50.00, 85.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(20.00, 70.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.95, 1.25));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-10, 10));
            },
            Biome::ColdBog => {
                self.characteristics.modify_available_land(rng.gen_range(-10.00, 0.00));
                self.characteristics.modify_forest_coverage(rng.gen_range(50.00, 90.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(20.00, 80.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.70, 1.00));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-20, 0));
            },
            Biome::MediterraneanWoods => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(60.00, 85.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(75.00, 95.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.95, 1.05));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(0, 15));
            },
            Biome::Ocean => {
                self.characteristics.modify_available_land(rng.gen_range(-100.00, -98.00));
                // self.characteristics.modify_forest_coverage(0.00);
                // self.characteristics.modify_arable_land_coverage(0.00);
                // self.characteristics.modify_fertility_rating(0.00);
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-25, 20));
            },
            Biome::Sea => {
                self.characteristics.modify_available_land(rng.gen_range(-100.00, -98.00));
                // self.characteristics.modify_forest_coverage(0.00);
                // self.characteristics.modify_arable_land_coverage(0.00);
                // self.characteristics.modify_fertility_rating(0.00);
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-15, 25));
            },
            Biome::Lake => {
                self.characteristics.modify_available_land(rng.gen_range(-100.00, -98.00));
                // self.characteristics.modify_forest_coverage(0.00);
                // self.characteristics.modify_arable_land_coverage(0.00);
                // self.characteristics.modify_fertility_rating(0.00);
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-10, 20));
            },
            Biome::Badlands => {
                // self.characteristics.modify_available_land(0.00);
                self.characteristics.modify_forest_coverage(rng.gen_range(0.00, 5.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(0.00, 20.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.00, 0.25));
                // self.characteristics.modify_altitude(0);
                self.characteristics.modify_temperature(rng.gen_range(-10, 20));
            },
            _ => {
                self.characteristics.modify_available_land(rng.gen_range(0.00, 0.00));
                self.characteristics.modify_forest_coverage(rng.gen_range(0.00, 0.00));
                self.characteristics.modify_arable_land_coverage(rng.gen_range(0.00, 0.00));
                self.characteristics.modify_fertility_rating(rng.gen_range(0.00, 0.00));
                self.characteristics.modify_altitude(rng.gen_range(0, 0));
                self.characteristics.modify_temperature(rng.gen_range(0, 0));
            }
        }

        for f in &self.features {
            match f {
                BiomeFeature::Hills => {
                    self.characteristics.modify_available_land(rng.gen_range(-10.00, 0.00));
                    self.characteristics.modify_altitude(1);
                },
                BiomeFeature::Highlands => {
                    self.characteristics.modify_available_land(rng.gen_range(-15.00, 0.00));
                    self.characteristics.modify_altitude(2);
                    self.characteristics.modify_temperature(rng.gen_range(-10, 0));
                },
                BiomeFeature::Mountains => {
                    self.characteristics.modify_available_land(rng.gen_range(-70.00, -30.00));
                    self.characteristics.modify_altitude(4);
                    self.characteristics.modify_temperature(rng.gen_range(-50, -5));
                },
                BiomeFeature::River => {
                    self.characteristics.modify_available_land(rng.gen_range(-10.00, -2.00));
                    self.characteristics.multiply_fertility_rating(rng.gen_range(1.10, 1.60));
                },
                BiomeFeature::Ravines => {
                    self.characteristics.modify_available_land(rng.gen_range(-5.00, 0.00));
                    self.characteristics.modify_altitude(-1);
                },
                BiomeFeature::Plateau => {
                    self.characteristics.modify_altitude(2);
                },
                BiomeFeature::Ponds => {
                    self.characteristics.modify_available_land(rng.gen_range(-25.00, -5.00));
                    self.characteristics.multiply_fertility_rating(rng.gen_range(1.05, 1.20));
                },
                BiomeFeature::Coastline => {
                    self.characteristics.modify_available_land(rng.gen_range(-50.00, -10.00));
                },
                _ => {}
            }
        }
    }
}

// Stores additional characteristics about a tile including its forest coverage, temperature, altitude, etc.
#[derive(Clone, Debug, PartialEq)]
pub struct TileCharacteristics {
    available_land: f64,

    forest_coverage: f64,
    arable_land_coverage: f64,

    fertility_rating: f64,

    altitude: i32,
    temperature: i32,
}

impl TileCharacteristics {
    // Creates a new TileCharacteristics from provided information.
    pub fn new(available: f64, forest:f64, arable: f64, fertility: f64, alt: i32, tmp: i32) -> TileCharacteristics {
        let tc = TileCharacteristics {
            available_land: available,

            forest_coverage: forest,
            arable_land_coverage: arable,
    
            fertility_rating: fertility,

            altitude: alt,
            temperature: tmp,
        };
        return tc;
    }

    pub fn default() -> TileCharacteristics {
        let tc = TileCharacteristics {
            available_land: TILE_SIZE,

            forest_coverage: 0.0,
            arable_land_coverage: 0.0,
    
            fertility_rating: 0.0,

            altitude: 1,
            temperature: 15,
        };
        return tc;
    }

    // Accessor method.
    pub fn available_land(&self) -> f64 {
        return self.available_land;
    }

    // Accessor method.
    pub fn forest_coverage(&self) -> f64 {
        return self.forest_coverage;
    }

    // Accessor method.
    pub fn arable_land_coverage(&self) -> f64 {
        return self.arable_land_coverage;
    }

    // Accessor method.
    pub fn fertility_rating(&self) -> f64 {
        return self.fertility_rating;
    }

    // Accessor method. Fetches the altitude.
    pub fn altitude(&self) -> i32 {
        return self.altitude;
    }

    // Accessor method. Fetches the temperature.
    pub fn temperature(&self) -> i32 {
        return self.temperature;
    }

    // Mutator method. Sets the altitude.
    pub fn set_altitude(&mut self, new_alt: i32){
        self.altitude = new_alt;
    }
    
    // Mutator method. Sets the temperature.
    pub fn set_temperature(&mut self, new_tmp: i32){
        self.temperature = new_tmp;
    }

    // Mutator method.
    pub fn modify_available_land(&mut self, change: f64){
        self.available_land += change;
    }

    // Mutator method.
    pub fn modify_forest_coverage(&mut self, change: f64){
        self.forest_coverage += change;
    }

    // Mutator method.
    pub fn modify_arable_land_coverage(&mut self, change: f64){
        self.arable_land_coverage += change;
    }

    // Mutator method.
    pub fn modify_fertility_rating(&mut self, change: f64){
        self.fertility_rating += change;
    }

    // Mutator method. Modifies the altitude by adding a positive or negative integer.
    pub fn modify_altitude(&mut self, change: i32){
        self.altitude += change;
    }

    // Mutator method. Modifies the temperature by adding a positive or negative integer.
    pub fn modify_temperature(&mut self, change: i32){
        self.temperature += change;
    }

    // Mutator method.
    pub fn multiply_available_land(&mut self, change: f64){
        self.available_land *= change;
    }

    // Mutator method.
    pub fn multiply_forest_coverage(&mut self, change: f64){
        self.forest_coverage *= change;
    }

    // Mutator method.
    pub fn multiply_arable_land_coverage(&mut self, change: f64){
        self.arable_land_coverage *= change;
    }

    // Mutator method.
    pub fn multiply_fertility_rating(&mut self, change: f64){
        self.fertility_rating *= change;
    }
}


// Returns a boolean for if two given tiles are adjacent.
pub fn are_adjacent(a: Tile, b: Tile) -> bool {
    let k = b.get_coordinates();
    if a.is_adjacent_to(k.0, k.1, k.2){
        return true;
    } else {
        return false;
    }
}

//        x+                       
//   z-        y-                      
//        <>          all tiles are point up hexagons 
//   y+        z+                          
//        x-                         