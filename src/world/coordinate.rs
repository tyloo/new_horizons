// A data type containing a hexagonal x, y, and z value.
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub struct Coordinate {
    x: i32,
    y: i32,
    z: i32,
}

impl Coordinate {
    // Creates a new Coordinate from three integers.
    pub fn new(x: i32, y: i32, z: i32) -> Coordinate {
        let new_coor = Coordinate {x, y, z};
        return new_coor;
    }

    // Creates a new Coordinate from a tuple containing three integers.
    pub fn from_tuple((x, y, z): (i32, i32, i32)) -> Coordinate {
        let new_coor = Coordinate {x, y, z};
        return new_coor;
    }

    // Creates a tuple from a Coordinate containing (x, y, z) values.
    pub fn as_tuple(&self) -> (i32, i32, i32) {
        return (self.x, self.y, self.z);
    }

    // Accessor method. Returns the x-value of the Coordinate.
    pub fn x(&self) -> i32 {
        return self.x;
    }

    // Accessor method. Returns the y-value of the Coordinate.
    pub fn y(&self) -> i32 {
        return self.y;
    }

    // Accessor method. Returns the z-value of the Coordinate.
    pub fn z(&self) -> i32 {
        return self.z;
    }
    
    // Mutator method. Sets the x-value of the Coordinate.
    pub fn set_x(&mut self, new_x: i32){
        self.x = new_x;
    }

    // Mutator method. Sets the y-value of the Coordinate.
    pub fn set_y(&mut self, new_y: i32){
        self.y = new_y;
    }

    // Mutator method. Sets the z-value of the Coordinate.
    pub fn set_z(&mut self, new_z: i32){
        self.z = new_z;
    }
}

// Creates a tuple of the coordinates for the tile directly east of the given coordinates.
pub fn get_e_tuple((x, y, z): (i32, i32, i32)) -> (i32, i32, i32) {
    return (x, y-1, z+1)
}

// Creates a tuple of the coordinates for the tile directly west of the given coordinates.
pub fn get_w_tuple((x, y, z): (i32, i32, i32)) -> (i32, i32, i32) {
    return (x, y+1, z-1)
}

// Creates a tuple of the coordinates for the tile directly northwest of the given coordinates.
pub fn get_nw_tuple((x, y, z): (i32, i32, i32)) -> (i32, i32, i32) {
    return (x+1, y, z-1)
}

// Creates a tuple of the coordinates for the tile directly southeast of the given coordinates.
pub fn get_se_tuple((x, y, z): (i32, i32, i32)) -> (i32, i32, i32) {
    return (x-1, y, z+1)
}

// Creates a tuple of the coordinates for the tile directly northeast of the given coordinates.
pub fn get_ne_tuple((x, y, z): (i32, i32, i32)) -> (i32, i32, i32) {
    return (x+1, y-1, z)
}

// Creates a tuple of the coordinates for the tile directly southwest of the given coordinates.
pub fn get_sw_tuple((x, y, z): (i32, i32, i32)) -> (i32, i32, i32) {
    return (x-1, y+1, z)
}

pub enum Direction {
    East,
    West,
    Northwest,
    Southeast,
    Northeast,
    Southwest,
}