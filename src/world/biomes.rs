extern crate rand;

use rand::*;

// Defines all of the standard biomes.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Biome {
    Empty,

    Tundra,
    Taiga,

    Forest, //Temperate Forest
    Woodland, //Meadow

    Chaparral, //Scrubland, Shrubland
    Plains,
    Savannah,

    Desert,
    Steppes,
    CoolDesert,

    TemperateRainForest,
    TropicalForest,
    Jungle, //TropicalRainForest

    TropicalSwamp,
    TemperateWetland,
    ColdBog,

    MediterraneanWoods,

    Badlands,

    Ocean,
    Sea,
    Lake,
}

// Generates a random biome from a randomly generated integer.
pub fn random_biome() -> Biome {
    let mut rng = thread_rng();
    match rng.gen_range(0, 21) {
        0 => Biome::Tundra,
        1 => Biome::Taiga,
        2 => Biome::Forest,
        3 => Biome::Woodland,
        4 => Biome::Chaparral,
        5 => Biome::Plains,
        6 => Biome::Savannah,
        7 => Biome::Desert,
        8 => Biome::Steppes,
        9 => Biome::CoolDesert,
        10 => Biome::TemperateRainForest,
        11 => Biome::TropicalForest,
        12 => Biome::Jungle,
        13 => Biome::TropicalSwamp,
        14 => Biome::TemperateWetland,
        15 => Biome::ColdBog,
        16 => Biome::MediterraneanWoods,
        17 => Biome::Badlands,

        18 => Biome::Ocean,
        19 => Biome::Sea,
        20 => Biome::Lake,
        _ => Biome::Empty,
    }
}

// Specific types of biomes or features that are spliced onto a tile to modify it.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum BiomeFeature {
    Hills,
    Highlands,
    Mountains,
    River,
    Ravines,
    Plateau,
    Ponds,
    Coastline,
}

// Various features of tiles.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum TileFeature {
    HotSprings,
}