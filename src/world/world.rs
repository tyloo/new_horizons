extern crate rand;

use crate::world::biomes::*;
use crate::world::coordinate::*;
use crate::world::resources::*;
use crate::world::tile::*;

use crate::utils::shuffle_and_sort::*;

use crate::civilization::settlement::*;
use crate::civilization::entity::*;

use std::collections::*;
use std::time::{Instant};

use rand::*;

const WORLD_GENERATION_DEBUG: bool = false;
const TILE_CREATION_DEBUG: bool = false;
const BIOME_SET_DEBUG: bool = false;

// Stores all of the information pertaining to the game world.
pub struct World {
    radius: i32,
    size: i32,
    tiles: HashMap<(i32, i32, i32), Tile>,
    // entities: Vec<Entity>,
    internal_set: VecDeque<(i32, i32, i32)>,
}

impl World {
    // Adds a given settlement at the given location.
    // If the location does not exist, adds a new tile to the map and places the settlement there.
    pub fn add_settlement_at(&mut self, (x, y, z): (i32, i32, i32), new_settle: Settlement){
        match self.tiles.get_mut(&(x, y, z)) {
            None => {
                self.tiles.insert((x, y, z), Tile::new(x, y, z, random_biome()));
                self.tiles.get_mut(&(x, y, z)).unwrap().add_settlement(new_settle);
            }
            Some(tile) => {
                tile.add_settlement(new_settle);
            }
        }
    }

    pub fn get_mut_settlement_at(&mut self, (x, y, z): (i32, i32, i32)) -> Option<&mut Settlement> {
        return match self.tiles.get_mut(&(x, y, z)) {
            Some(tile) => tile.get_mut_settlement(),
            None => None,
        }
    }

    // pub fn add_entity(&mut self, entity: Entity){
    //     self.entities.push(entity);
    // }

    // pub fn get_entities(&self) -> &Vec<Entity> {
    //     return &self.entities;
    // }

    // pub fn get_mut_entities(&mut self) -> &mut Vec<Entity> {
    //     return &mut self.entities;
    // }

    // pub fn entity_count(&self) -> usize {
    //     return self.entities.len();
    // }

    pub fn get_tile_at(&self, (x, y, z): (i32, i32, i32)) -> Option<&Tile> {
        return self.tiles.get(&(x, y, z));
    }

    pub fn get_mut_tile_at(&mut self, (x, y, z): (i32, i32, i32)) -> Option<&mut Tile> {
        return self.tiles.get_mut(&(x, y, z));
    }

    pub fn check_and_generate(&mut self, coordinate: Coordinate){
        match self.tiles.get(&coordinate.as_tuple()) {
            Some(_) => {
                // If this tile is already initialized, only check the adjcent tiles.
                let mut new_tiles = VecDeque::new();

                // Adds all of the adjacent uninitialized tiles to the VecDeque as well.
                let mut adjacent_coordinates = Vec::<(i32, i32, i32)>::new();
                adjacent_coordinates.push(get_e_tuple(coordinate.as_tuple()));
                adjacent_coordinates.push(get_se_tuple(coordinate.as_tuple()));
                adjacent_coordinates.push(get_sw_tuple(coordinate.as_tuple()));
                adjacent_coordinates.push(get_w_tuple(coordinate.as_tuple()));
                adjacent_coordinates.push(get_nw_tuple(coordinate.as_tuple()));
                adjacent_coordinates.push(get_ne_tuple(coordinate.as_tuple()));
                for adjacent in adjacent_coordinates {
                    match self.tiles.get(&adjacent) {
                        Some(_) => {}
                        None => {
                            self.tiles.insert(adjacent, Tile::new(adjacent.0, adjacent.1, adjacent.2, Biome::Empty));
                            new_tiles.push_back(adjacent);
                        }
                    }
                }
                
                // Then populates the newly created tiles.
                self.populate_biomes_over(new_tiles.clone());
                self.populate_features_over(new_tiles.clone());
                self.adjust_tile_characteristics_over(new_tiles.clone());
                self.populate_resources_over(new_tiles.clone());
                
                self.internal_set.append(&mut new_tiles);
            },
            None => {
                self.tiles.insert(coordinate.as_tuple(), Tile::new(coordinate.x(), coordinate.y(), coordinate.z(), Biome::Empty));

                let mut new_tiles = VecDeque::new();
                new_tiles.push_back(coordinate.as_tuple());

                // Adds all of the adjacent uninitialized tiles to the VecDeque as well.
                let mut adjacent_coordinates = Vec::<(i32, i32, i32)>::new();
                adjacent_coordinates.push(get_e_tuple(coordinate.as_tuple()));
                adjacent_coordinates.push(get_se_tuple(coordinate.as_tuple()));
                adjacent_coordinates.push(get_sw_tuple(coordinate.as_tuple()));
                adjacent_coordinates.push(get_w_tuple(coordinate.as_tuple()));
                adjacent_coordinates.push(get_nw_tuple(coordinate.as_tuple()));
                adjacent_coordinates.push(get_ne_tuple(coordinate.as_tuple()));
                for adjacent in adjacent_coordinates {
                    match self.tiles.get(&adjacent) {
                        Some(_) => {}
                        None => {
                            self.tiles.insert(adjacent, Tile::new(adjacent.0, adjacent.1, adjacent.2, Biome::Empty));
                            new_tiles.push_back(adjacent);
                        }
                    }
                }

                // Then populates the newly created tiles.
                self.populate_biomes_over(new_tiles.clone());
                self.populate_features_over(new_tiles.clone());
                self.adjust_tile_characteristics_over(new_tiles.clone());
                self.populate_resources_over(new_tiles.clone());
                
                self.internal_set.append(&mut new_tiles);
            }
        };
    }

    pub fn get_internal_set_clone(&self) -> VecDeque<(i32, i32, i32)> {
        return self.internal_set.clone();
    }

    // Creates a new world with a given hexagonal radius.
    pub fn new_with_radius(radius: i32) -> World {
        let s = calc_size(radius);
        let mut w = World {
            radius: radius,
            size: s,
            tiles: HashMap::with_capacity(s as usize),
            // entities: Vec::new(),
            internal_set: VecDeque::with_capacity(s as usize),
        };
        w.generate();
        return w;
    }

    // Generates the world's terrain. Private. Called during the creation of the world or by the regenerate() function.
    fn generate(&mut self) -> Result<bool, bool> {
        let start = Instant::now();
        if WORLD_GENERATION_DEBUG {println!("Started world generation...");}

        let mut count = 0;  // Tracks the number of tiles created.

        let n: i32 = self.radius+1;
        let r: i32 = self.radius;

        let mut z = 0;
        let mut y = r;

        let mut length = n;

        // A set of all initialized coordinates.
        let mut set: VecDeque<(i32, i32, i32)> = VecDeque::new();

        for x in -r..n {
            for k in 0..length {
                self.tiles.insert((x, y-k, z+k), Tile::new(x, y-k, z+k, Biome::Empty));
                set.push_back((x, y-k, z+k));
                count += 1;
                if TILE_CREATION_DEBUG {
                    println!("Added tile at ({}, {}, {}).", x, y-k, z+k);
                }
            }

            if z > -r {
                z = z-1;
            }

            if x >= 0 && x < r {
                y = y-1;
            }

            if x < 0 {
                length = length+1;
            } else {
                length = length-1;
            }
        }
        if WORLD_GENERATION_DEBUG {println!("Finished tile generation after {} milliseconds!", start.elapsed().as_millis());}
        if WORLD_GENERATION_DEBUG {println!("   Asked for {} tiles. Generated {} tiles.", self.size, count);}

        // println!("Before shuffle: {:?}", set.clone());
        shuffle(&mut set);
        if WORLD_GENERATION_DEBUG {println!("Finished suffle after {} milliseconds.", start.elapsed().as_millis());}
        // println!("After shuffle: {:?}", set.clone());
        self.populate_biomes_over(set.clone());
        if WORLD_GENERATION_DEBUG {println!("Finished biome population after {} milliseconds.", start.elapsed().as_millis());}
        self.populate_features_over(set.clone());
        if WORLD_GENERATION_DEBUG {println!("Finished feature population after {} milliseconds.", start.elapsed().as_millis());}
        self.adjust_tile_characteristics_over(set.clone());
        if WORLD_GENERATION_DEBUG {println!("Finished tile characteristic adjustment after {} milliseconds.", start.elapsed().as_millis());}
        self.populate_resources_over(set.clone());
        if WORLD_GENERATION_DEBUG {println!("Finished resource population after {} milliseconds.", start.elapsed().as_millis());}

        self.internal_set = set.clone();

        return Ok(true);
    }

    // Randomly generates biomes for each tile in the VecDeque of coordinates.
    // Biomes have a 1/7 chance to copy the biome of each adjacent tile.
    // Biomes also have a 1/7 chance to be a completely random biome.
    fn populate_biomes_over(&mut self, mut vec: VecDeque<(i32, i32, i32)>){
        let mut rng = thread_rng();

        let mut count = 0;

        // the new biome generation system only checks nearby initialized tiles instead of all
        // nearby tiles. nearby uninitialized tiles will no longer increase the probability of
        // the tile being a random biome.
        // if true, use the new biome randomization system
        if true {
            for coordinate in vec {
                let mut options = vec!(Biome::Empty);
    
                let mut adjacent_coordinates = Vec::<(i32, i32, i32)>::new();
                adjacent_coordinates.push(get_e_tuple(coordinate));
                adjacent_coordinates.push(get_se_tuple(coordinate));
                adjacent_coordinates.push(get_sw_tuple(coordinate));
                adjacent_coordinates.push(get_w_tuple(coordinate));
                adjacent_coordinates.push(get_nw_tuple(coordinate));
                adjacent_coordinates.push(get_ne_tuple(coordinate));
                for adjacent in adjacent_coordinates {
                    match self.tiles.get(&adjacent) {
                        Some(tile) => {
                            options.push(tile.get_biome());
                        }
                        None => {}
                    }
                }

                let mut b = options[rng.gen_range(0, options.len())].clone();
                if b == Biome::Empty {
                    b = random_biome();
                }
                if BIOME_SET_DEBUG {println!("[{}] {:?} is now {:?} using new biome population.", count, coordinate, b.clone());}
                self.tiles.get_mut(&coordinate).unwrap().change_biome(b);

                count += 1;
            }
        } else {
            while !vec.is_empty() {
                let c = vec.pop_front().unwrap();
                let mut b: Biome;
                match rng.gen_range(0, 7) {
                    0 => {
                        b = match self.tiles.get(&get_e_tuple(c)) {
                            Some(tile) => tile.get_biome(),
                            None => Biome::Empty,
                        };
                        let temp = b.clone();
                        if b == Biome::Empty {
                            b = random_biome();
                        }
                        if BIOME_SET_DEBUG {println!("[{}] East tile is {:?}. {:?} is now {:?}.", count, temp, c, b.clone());}
                        self.tiles.get_mut(&c).unwrap().change_biome(b);
                    },
                    1 => {
                        b = match self.tiles.get(&get_w_tuple(c)) {
                            Some(tile) => tile.get_biome(),
                            None => Biome::Empty,
                        };
                        let temp = b.clone();
                        if b == Biome::Empty {
                            b = random_biome();
                        }
                        if BIOME_SET_DEBUG {println!("[{}] West tile is {:?}. {:?} is now {:?}.", count, temp, c, b.clone());}
                        self.tiles.get_mut(&c).unwrap().change_biome(b);
                    },
                    2 => {
                        b = match self.tiles.get(&get_nw_tuple(c)) {
                            Some(tile) => tile.get_biome(),
                            None => Biome::Empty,
                        };
                        let temp = b.clone();
                        if b == Biome::Empty {
                            b = random_biome();
                        }
                        if BIOME_SET_DEBUG {println!("[{}] Northwest tile is {:?}. {:?} is now {:?}.", count, temp, c, b.clone());}
                        self.tiles.get_mut(&c).unwrap().change_biome(b);
                    },
                    3 => {
                        b = match self.tiles.get(&get_se_tuple(c)) {
                            Some(tile) => tile.get_biome(),
                            None => Biome::Empty,
                        };
                        let temp = b.clone();
                        if b == Biome::Empty {
                            b = random_biome();
                        }
                        if BIOME_SET_DEBUG {println!("[{}] Southeast tile is {:?}. {:?} is now {:?}.", count, temp, c, b.clone());}
                        self.tiles.get_mut(&c).unwrap().change_biome(b);
                    },
                    4 => {
                        b = match self.tiles.get(&get_ne_tuple(c)) {
                            Some(tile) => tile.get_biome(),
                            None => Biome::Empty,
                        };
                        let temp = b.clone();
                        if b == Biome::Empty {
                            b = random_biome();
                        }
                        if BIOME_SET_DEBUG {println!("[{}] Northeast tile is {:?}. {:?} is now {:?}.", count, temp, c, b.clone());}
                        self.tiles.get_mut(&c).unwrap().change_biome(b);
                    },
                    5 => {
                        b = match self.tiles.get(&get_sw_tuple(c)) {
                            Some(tile) => tile.get_biome(),
                            None => Biome::Empty,
                        };
                        let temp = b.clone();
                        if b == Biome::Empty {
                            b = random_biome();
                        }
                        if BIOME_SET_DEBUG {println!("[{}] Southwest tile is {:?}. {:?} is now {:?}.", count, temp, c, b.clone());}
                        self.tiles.get_mut(&c).unwrap().change_biome(b);
                    },
                    6 => {
                        b = random_biome();
                        if BIOME_SET_DEBUG {println!("[{}] New random biome. {:?} is now {:?}.", count, c, b.clone());}
                        self.tiles.get_mut(&c).unwrap().change_biome(b);
                    },
                    _ => {}
                }
            }

            count += 1;
        }
    }

    // Randomly generates a number of BiomeFeature for each tile in a VecDeque of coordinates. 
    // Note that most tiles will be featureless.
    fn populate_features_over(&mut self, mut vec: VecDeque<(i32, i32, i32)>) {
        let mut rng = thread_rng();

        let mut count = 0;

        while !vec.is_empty() {
            let coordinate = vec.pop_front().unwrap();
            let t = self.tiles.get_mut(&coordinate).unwrap();

            if t.get_biome() != Biome::Lake  && 
               t.get_biome() != Biome::Sea   && 
               t.get_biome() != Biome::Ocean {
                match rng.gen_range(0, 100) {
                    0..=9 => {
                        t.add_biome_feature(BiomeFeature::Mountains);
                    }, // 10%
                    10..=24 => {
                        let t = self.tiles.get_mut(&coordinate).unwrap();
                        t.add_biome_feature(BiomeFeature::Hills);
                    }, // 15%
                    25..=29 => {
                        let t = self.tiles.get_mut(&coordinate).unwrap();
                        t.add_biome_feature(BiomeFeature::Highlands);
                    }, //  5%
                    30..=31 => {
                        let t = self.tiles.get_mut(&coordinate).unwrap();
                        t.add_biome_feature(BiomeFeature::Ravines);
                    }, //  2%
                    32..=33 => {
                        let t = self.tiles.get_mut(&coordinate).unwrap();
                        t.add_biome_feature(BiomeFeature::Plateau);
                    }, //  2%
                    40..=44 => {
                        let t = self.tiles.get_mut(&coordinate).unwrap();
                        t.add_biome_feature(BiomeFeature::Ponds);
                    }, //  5%
                    _ => {}
                }
            }
        }
    }

     // Adjusts TileCharacteristics for each tile in the set.
    // Adjustments are based on biome and features.
    fn adjust_tile_characteristics_over(&mut self, mut vec: VecDeque<(i32, i32, i32)>){
        while !vec.is_empty() {
            let c = vec.pop_front().unwrap();
            let t = self.tiles.get_mut(&c).unwrap();
            t.build_characteristics();
        }
    }


    // Adds resources on each tile with a coordinate in the VecDeque.
    // Adds some guaranteed resources based on the tile's biome type (ex. Wood for Forests).
    // Adds some random resources.
    fn populate_resources_over(&mut self, mut vec: VecDeque<(i32, i32, i32)>) {
        let mut rng = thread_rng();

        let mut count = 0;

        while !vec.is_empty() {
            let c = vec.pop_front().unwrap();
            
            let mut n = 0;
            let mut enough = false;

            while !enough {
                n += 1;
                match rng.gen_range(0, n+1) {
                    0 => {}
                    _ => enough = true,
                }
            }

            // Guaranteed Resources
            match self.tiles.get(&c).unwrap().get_biome() {
                Biome::Tundra => {

                },
                Biome::Taiga => {
                    
                },
                Biome::Forest => {

                },
                Biome::Woodland => {

                },
                Biome::Chaparral => {

                },
                Biome::Plains => {

                },
                Biome::Savannah => {

                },
                Biome::Desert => {

                },
                Biome::Steppes => {

                },
                Biome::CoolDesert => {

                },
                Biome::TemperateRainForest => {
                    
                },
                Biome::TropicalForest => {
                    
                },
                Biome::Jungle => {
                    
                },
                Biome::TropicalSwamp => {

                },
                Biome::TemperateWetland => {

                },
                Biome::ColdBog => {

                },
                Biome::MediterraneanWoods => {
                    
                },
                Biome::Ocean => {

                },
                Biome::Sea => {

                },
                Biome::Lake => {

                },
                Biome::Badlands => {

                },
                _ => {}
            }

            // Random Resources by Biome
            match self.tiles.get(&c).unwrap().get_biome() {
                _ => {}
            }

            // Random Resources by Features
            let bf = self.tiles.get(&c).unwrap().get_biome_features();
            for feature in bf {
                if feature == BiomeFeature::Mountains {
                    // add extra ore...?
                }
            }
            //out
        }
    }
    
    // A public function which calls the internal world generation. 
    // Only call when explicitly regenerating the entire world.
    pub fn regenerate(&mut self) -> Result<bool, bool> {
        return self.generate();
    }

    // A test function.
    // Changes a few biomes and then attempts to read the biomes at those locations. 
    // Should pass easily.
    pub fn run_biome_test(&mut self) {
        let t = self.tiles.get_mut(&(-1, 1, 0)).unwrap();
        println!("Changed {:?} at (-1, 1, 0) to a Taiga.", t.get_biome());
        t.change_biome(Biome::Taiga);
        drop(t);

        let t = self.tiles.get_mut(&(1, -1, 0)).unwrap();
        println!("Changed {:?} at (1, -1, 0) to a Desert.", t.get_biome());
        t.change_biome(Biome::Desert);
        drop(t);

        let t = self.tiles.get_mut(&(-1, 1, 0)).unwrap(); 
        println!("Read (-1, 1, 0) and found a {:?}", t.get_biome());
        drop(t);

        let t = self.tiles.get_mut(&(1, -1, 0)).unwrap(); 
        println!("Read (1, -1, 0) and found a {:?}", t.get_biome());
        drop(t);
    }
}

// Calculates the number of tiles required to fill a hexagonal map with the given radius.
fn calc_size(radius: i32) -> i32 {
    let mut r = radius;
    let mut c = 1;
    while r > 0 {
        c += r*6;
        r -= 1;
    }
    return c;
}

// prints a whole ton of useful information about a given tile to the terminal. 
pub fn print_survey(world: &World, coordinate: Coordinate) -> () {
    match world.get_tile_at(coordinate.as_tuple()) {
        Some(tile) => {
            println!("   The tile is a {:?} biome.", tile.get_biome());
            println!("   The tile has {:?}.", tile.get_biome_features());
            println!("   The tile has {:.4}% available land.", tile.get_biome_characteristics().available_land());
            println!("   The tile has {:.4}% forest coverage.", tile.get_biome_characteristics().forest_coverage());
            println!("   The tile has {:.4}% arable land coverage.", tile.get_biome_characteristics().arable_land_coverage());
            println!("   The tile has {:.4}% fertility rating.", tile.get_biome_characteristics().fertility_rating()*100.0);
            println!("   The tile has an altitude of {:?}.", tile.get_biome_characteristics().altitude());
            println!("   The tile has a temperature of {:?}.", tile.get_biome_characteristics().temperature());
        },
        None => {
            println!("   There is not tile at the given coordinates! Unable to complete survey.")
        }
    };
}