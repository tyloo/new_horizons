use crate::civilization::unit::*;

use crate::world::coordinate::*;
use crate::world::world::*;

use std::collections::{HashMap};

#[derive(Clone)]
pub struct EntityList {
    pub list: Vec<Entity>,
    pub aliases: HashMap<String, usize>,
    pub locations: HashMap<Coordinate, Vec<String>>,
}

impl EntityList {
    pub fn new() -> EntityList {
        return EntityList {
            list: Vec::new(),
            aliases: HashMap::new(),
            locations: HashMap::new(),
        };
    }

    pub fn add_entity(&mut self, mut entity: Entity) -> (String, usize){
        // Initializes the ID for the unit.
        let mut id = (String::from(""), 0 as usize);

        // Appends (+) to the alias if it already exists, then checks again.
        while self.aliases.contains_key(&entity.alias) {
            entity.alias.push_str("(+)");
        }
        // Updates the ID with the new alias.
        id.0 = entity.alias.clone();

        // Updates the ID with the new index (i.e. the next available index in the Vec).
        id.1 = self.list.len();

        // Inserts the completed ID into the alias map.
        self.aliases.insert(id.0.clone(), id.1);

        // Inserts the entity's alias into the locations HashMap, modifying the entry or creating
        // a new one as needed.
        if self.locations.contains_key(&entity.location()) {
            self.locations.get_mut(&entity.location())
                .unwrap() // this unwrap should never fail if locations contains the key
                .push(id.0.clone());
        } else {
            self.locations.insert(entity.location().clone(), vec!(id.0.clone()));
        }

        // Pushes the new entity to the list.
        self.list.push(entity);

        // Returns the ID for easy access to the entity.
        return id;
    }

    pub fn get_entities(&self) -> &Vec<Entity> {
        return &self.list;
    }

    pub fn get_mut_entities(&mut self) -> &mut Vec<Entity> {
        return &mut self.list;
    }

    pub fn get_entity_with_index(&self, index: usize) -> &Entity {
        return &self.list[index];
    }

    pub fn get_mut_entity_with_index(&mut self, index: usize) -> &mut Entity {
        return &mut self.list[index];
    }

    pub fn get_entity_with_alias(&self, alias: String) -> &Entity {
        let i = *self.aliases.get(&alias).unwrap();
        return &self.list[i];
    }

    pub fn get_mut_entity_with_alias(&mut self, alias: String) -> &mut Entity {
        let i = *self.aliases.get(&alias).unwrap();
        return &mut self.list[i];
    }

    pub fn get_aliases_at(&self, coordinate: Coordinate) -> Vec<String> {
        match self.locations.get(&coordinate) {
            Some(vec) => {
                return vec.clone();
            },
            None => {
                return Vec::<String>::new();
            }
        }
    }

    //      --- A C T I O N S ---
    pub fn move_aliased_entity(&mut self, world: &mut World, alias: String, direction: Direction) -> Result<Coordinate, Coordinate> {
        // Stores a mutable reference to the entity with the given alias and stores its initial location.
        let entity = self.get_mut_entity_with_alias(alias.clone());
        let previous_location = entity.location();

        // Gives the entity a move command based on the provided Direction.
        let new_location = match direction {
            Direction::East => entity.move_east(world).unwrap(),
            Direction::West => entity.move_west(world).unwrap(),
            Direction::Northwest => entity.move_northwest(world).unwrap(),
            Direction::Southeast => entity.move_southeast(world).unwrap(),
            Direction::Northeast => entity.move_northeast(world).unwrap(),
            Direction::Southwest => entity.move_southwest(world).unwrap(),
        };

        // Removes the entity's alias from the vector associated with the entity's previous location.
        let alias_set = self.locations.get_mut(&previous_location).unwrap();
        for i in 0..alias_set.len() {
            if alias_set[i] == alias {
                alias_set.remove(i);
            }
        }

        // Inserts the entity's alias into the vector associated with the entities new location,
        // creating a new HashMap as needed.
        match self.locations.get_mut(&new_location) {
            Some(vec) => {
                vec.push(alias);
            },
            None => {
                self.locations.insert(new_location.clone(), vec!(alias));
            }
        }

        return Ok(new_location);
    }
}

#[derive(Clone)]
pub enum EntityType {
    Generic,
    Military,
    Civilian,
    Character,
}

#[derive(Clone)]
pub struct Entity {
    entity_name: String,
    alias: String,
    units: Vec<UnitGroup>,
    entity_type: EntityType,
    location: Coordinate,
}

impl Entity {
    pub fn new_at(new_name: String, new_alias: String, x: i32, y: i32, z: i32) -> Entity {
        let new_entity = Entity {
            entity_name: new_name,
            alias: new_alias,
            units: Vec::new(),
            entity_type: EntityType::Generic,
            location: Coordinate::new(x, y, z),
        };
        return new_entity;
    }
    pub fn add_unit(&mut self, unit: UnitGroup) {
        self.units.push(unit);
    }

    pub fn name(&self) -> String {
        return self.entity_name.clone();
    }

    pub fn location(&self) -> Coordinate {
        return self.location.clone();
    }

    //      --- A C T I O N S ---
    // When moving, use world::world::world::check_and_generate() on own location.
    pub fn move_east(&mut self, world: &mut World) -> Result<Coordinate, Coordinate> {
        let mut new_loc = (self.location.x() +0, self.location.y() -1, self.location.z() +1);
        self.location = Coordinate::from_tuple(new_loc);
        world.check_and_generate(self.location());
        return Ok(Coordinate::from_tuple(new_loc));
    }
    pub fn move_west(&mut self, world: &mut World) -> Result<Coordinate, Coordinate> {
        let mut new_loc = (self.location.x() +0, self.location.y() +1, self.location.z() -1);
        self.location = Coordinate::from_tuple(new_loc);
        world.check_and_generate(self.location());
        return Ok(Coordinate::from_tuple(new_loc));
    }
    pub fn move_northwest(&mut self, world: &mut World) -> Result<Coordinate, Coordinate> {
        let mut new_loc = (self.location.x() +1, self.location.y() +0, self.location.z() -1);
        self.location = Coordinate::from_tuple(new_loc);
        world.check_and_generate(self.location());
        return Ok(Coordinate::from_tuple(new_loc));
    }
    pub fn move_southeast(&mut self, world: &mut World) -> Result<Coordinate, Coordinate> {
        let mut new_loc = (self.location.x() -1, self.location.y() +0, self.location.z() +1);
        self.location = Coordinate::from_tuple(new_loc);
        world.check_and_generate(self.location());
        return Ok(Coordinate::from_tuple(new_loc));
    }
    pub fn move_northeast(&mut self, world: &mut World) -> Result<Coordinate, Coordinate> {
        let mut new_loc = (self.location.x() +1, self.location.y() -1, self.location.z() +0);
        self.location = Coordinate::from_tuple(new_loc);
        world.check_and_generate(self.location());
        return Ok(Coordinate::from_tuple(new_loc));
    }
    pub fn move_southwest(&mut self, world: &mut World) -> Result<Coordinate, Coordinate> {
        let mut new_loc = (self.location.x() -1, self.location.y() +1, self.location.z() +0);
        self.location = Coordinate::from_tuple(new_loc);
        world.check_and_generate(self.location());
        return Ok(Coordinate::from_tuple(new_loc));
    }

    pub fn survey_here(&self, world: &World) -> () {
        println!("{} completed survey at {:?}.", self.entity_name, self.location.as_tuple());
        print_survey(world, self.location.clone());
    }
}

