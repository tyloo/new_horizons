use crate::civilization::character::*;

// UnitGroup is kind of a messy data type. It has two variants: Individual and Group.
// The Individual variant has an associated Character and represents a specific individual who is
// part of the larger Entity.
// The Group variant has an associated Unit and integer value. A Unit represents a generalized 
// archetype about what someone or something is. Some common Units might be a "Medium Infantry"
// Unit or a "Ranger" Unit. Because they represent general archetypes, one can have a UnitGroup
// made up of 50 Medium Infantry, while you'll only ever have one "King Arthur" Individual/Character.
#[derive(Clone)]
pub enum UnitGroup {
    Individual(Character),
    Group(Unit, u32),
}

#[derive(Clone)]
pub enum UnitClass {
    Civilian,
    Trader,

    LightInfantry,
}

#[derive(Clone)]
pub struct Unit {
    combat_strength: u64, // NOTE: not all Units are combat units, but all units have a combat strength value (default: 0)
    // combat strength is calculated based on a Unit's equipment and type
    equipment: Vec<Equipment>
}

impl Unit {
    pub fn new() -> Unit {
        let new_unit = Unit {
            combat_strength: 0,
            equipment: Vec::new(),
        };
        return new_unit;
    }
}

#[derive(Clone)]
pub struct Equipment {

}