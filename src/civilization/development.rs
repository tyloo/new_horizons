use crate::world::coordinate::*;

#[derive(Clone)]
pub enum DevelopmentType {
    Empty,

    ForestryCamp(u32), // labor -> wood
    LumberMill(u32),   // wood -> lumber

    Ironworks(u32),    // ironore -> iron

    CivilianAgriculture(u32),
    Farm(u32), // labor -> crops~

    // THESE WILL BE REWORKED, I'M REIMAGINING HOW BIG A SINGLE TILE IS...
}

#[derive(Clone)]
pub struct TileDevelopment {
    location: Coordinate,
    name: String,
    development_type: DevelopmentType,
}

impl TileDevelopment {
    pub fn new_at(x: i32, y: i32, z: i32, name: String, development_type: DevelopmentType) -> TileDevelopment {
        let new_development = TileDevelopment {
            location: Coordinate::new(x, y, z),
            name,
            development_type,
        };
        return new_development;
    }
}

// Specific constructions.
pub fn new_forestry_camp(x: i32, y: i32, z: i32, name: String) -> TileDevelopment {
    let forestry = TileDevelopment::new_at(x, y, z, name, DevelopmentType::ForestryCamp(1));
    return forestry;
}