use crate::civilization::buildings::*;
use crate::civilization::people::*;
use crate::civilization::labor::*;
use crate::world::coordinate::*;
use crate::world::resources::*;

use std::collections::HashMap;

const JOB_REGISTRY_DEBUG: bool = false;
const LABOR_MANIFEST_DEBUG: bool = false;
const PRODUCTION_FROM_MANIFEST_DEBUG: bool = false;

#[derive(Clone, Debug)]
pub enum SettlementLevel {
    Outpost,
    FrontierSettlement,
    Village,
    TownCenter,
    TownDistrict,
    CityCenter,
    CityDistrict,
    Castle,
}

#[derive(Clone, Debug)]
pub struct SettlementCharacteristics {
    housing: i64,
    amenities: i64,
    education: i64,
    protection: i64,
    medical: i64,
}

#[derive(Clone, Debug)]
pub struct LandUse {
    land_area_used: f64,
    forest_coverage_worked: f64,
    arable_land_worked: f64,
}

pub struct Settlement {
    name: String,
    location: Coordinate,
    level: SettlementLevel,
    population: Population,
    buildings: Vec<SettlementBuilding>,
    // land_use: LandUse,
    stockpile: Stockpile,

    // TODO: implement land use
    // TODO: add characteristics and demands

    job_registry: JobRegistry,      // tracks all jobs for the settlement and associated region
    labor_manifest: Vec<String>,    // tracks completed labor requests by their unique work_id
}

impl Settlement {
    pub fn new(name: String, level: SettlementLevel, x: i32, y: i32, z: i32, initial_pop: u64) -> Settlement {
        let new_settle = Settlement {
            name,
            location: Coordinate::new(x, y, z),
            level,
            population: Population::new(initial_pop),
            buildings: Vec::new(),
            stockpile: Stockpile::new(),
            job_registry: JobRegistry::new(),
            labor_manifest: Vec::new(),
        };
        return new_settle;
    }

    pub fn name(&self) -> String {
        return self.name.clone();
    }

    pub fn add_building(&mut self, new_building: SettlementBuilding) {
        self.buildings.push(new_building);
    }

    pub fn get_buildings(&self) -> &Vec<SettlementBuilding> {
        return &self.buildings;
    }

    pub fn get_mut_buildings(&mut self) -> &mut Vec<SettlementBuilding> {
        return &mut self.buildings;
    }

    pub fn consume_food(&mut self) {
        self.stockpile.withdraw(Resource::Grains(GrainType::Oats), self.population.size() as f64 / 4.0);
    }

    pub fn refresh_job_list(&mut self) {
        self.job_registry.empty();

        for building in &self.buildings {
            for request in building.get_jobs() {
                self.job_registry.add_request(request);
            }
        }

        self.job_registry.priority_last_sort();

        if JOB_REGISTRY_DEBUG { // for DEBUGGING purposes only
            println!("JOB REGISTRY: {:?}", self.job_registry);
        }
    }

    pub fn distribute_labor(&mut self) {
        let mut available_workers = self.population.get_workers();

        while !self.job_registry.is_empty() {       // as long as there are requests in the registry
            match self.job_registry.pop_request() { // get the first request
                Some(request) => {                  // unwrap that request
                    let mut workers_needed = request.workers_needed();

                    match request.work_type() {     // match on the WorkType of that request
                        WorkType::Farming => {      // for Farming work
                            if available_workers.hashmap.get(&WorkType::Farming).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Farming).unwrap_or(&0) > &0 {  // assign farmers
                                        available_workers.hashmap.insert(WorkType::Farming, available_workers.hashmap.get(&WorkType::Farming).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO FARMING! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::Forestry => {      // for Forestry work
                            if available_workers.hashmap.get(&WorkType::Forestry).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Forestry).unwrap_or(&0) > &0 {  // assign woodcutters
                                        available_workers.hashmap.insert(WorkType::Forestry, available_workers.hashmap.get(&WorkType::Forestry).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO Forestry! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::Shepherd => {      // for Shepherding work
                            if available_workers.hashmap.get(&WorkType::Shepherd).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Shepherd).unwrap_or(&0) > &0 {  // assign woodcutters
                                        available_workers.hashmap.insert(WorkType::Shepherd, available_workers.hashmap.get(&WorkType::Shepherd).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO SHEPHERD! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::BasicConstruction => {
                            if available_workers.hashmap.get(&WorkType::BasicConstruction).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::BasicConstruction).unwrap_or(&0) > &0 { // assign builders
                                        available_workers.hashmap.insert(WorkType::BasicConstruction, available_workers.hashmap.get(&WorkType::BasicConstruction).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                                // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO BasicConstruction! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::LightIndustry => {      // for LightIndustry work
                            if available_workers.hashmap.get(&WorkType::LightIndustry).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::LightIndustry).unwrap_or(&0) > &0 {  // assign light industry workers
                                        available_workers.hashmap.insert(WorkType::LightIndustry, available_workers.hashmap.get(&WorkType::LightIndustry).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO LightIndustry! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::Domestic => {      // for Domestic work
                            if available_workers.hashmap.get(&WorkType::Domestic).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Domestic).unwrap_or(&0) > &0 {  // assign domestic workers
                                        available_workers.hashmap.insert(WorkType::Domestic, available_workers.hashmap.get(&WorkType::Domestic).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO Domestic! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::Librarian => {      // for Librarian work
                            if available_workers.hashmap.get(&WorkType::Librarian).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Librarian).unwrap_or(&0) > &0 {  // assign librarians
                                        available_workers.hashmap.insert(WorkType::Librarian, available_workers.hashmap.get(&WorkType::Librarian).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO Librarian! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::Medical => {      // for Medical work
                            if available_workers.hashmap.get(&WorkType::Medical).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Medical).unwrap_or(&0) > &0 {  // assign medical workers
                                        available_workers.hashmap.insert(WorkType::Medical, available_workers.hashmap.get(&WorkType::Medical).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO Medical! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::Education => {      // for Education work
                            if available_workers.hashmap.get(&WorkType::Education).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Education).unwrap_or(&0) > &0 {  // assign Education workers
                                        available_workers.hashmap.insert(WorkType::Education, available_workers.hashmap.get(&WorkType::Education).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO Education! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::Mayor => {      // for Mayor work
                            if available_workers.hashmap.get(&WorkType::Mayor).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Mayor).unwrap_or(&0) > &0 {  // assign Mayor workers
                                        available_workers.hashmap.insert(WorkType::Mayor, available_workers.hashmap.get(&WorkType::Mayor).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO Mayor! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::Clerk => {      // for Clerk work
                            if available_workers.hashmap.get(&WorkType::Clerk).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Clerk).unwrap_or(&0) > &0 {  // assign Clerk workers
                                        available_workers.hashmap.insert(WorkType::Clerk, available_workers.hashmap.get(&WorkType::Clerk).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO Clerk! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::Hunting => {      // for Hunting work
                            if available_workers.hashmap.get(&WorkType::Hunting).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Hunting).unwrap_or(&0) > &0 {  // assign Hunting workers
                                        available_workers.hashmap.insert(WorkType::Hunting, available_workers.hashmap.get(&WorkType::Hunting).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO Hunting! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::Processing => {      // for Processing work
                            if available_workers.hashmap.get(&WorkType::Processing).unwrap_or(&0) + available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Processing).unwrap_or(&0) > &0 {  // assign Processing workers
                                        available_workers.hashmap.insert(WorkType::Processing, available_workers.hashmap.get(&WorkType::Processing).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    } else {                            // otherwise assign unemployed
                                        available_workers.hashmap.insert(WorkType::Unemployed, available_workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                        // TODO: MOVE A WORKER FROM UNEMPLOYED TO Processing! GIVE THEM A JOB!
                                    }
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        WorkType::Clergy => {      // for Clergy work
                            if available_workers.hashmap.get(&WorkType::Clergy).unwrap_or(&0) + 0 >= workers_needed {
                                while workers_needed > 0 {  // pretty inefficient, needs lots of compares, TODO: reduce number of compares with some simple math
                                    if available_workers.hashmap.get(&WorkType::Clergy).unwrap_or(&0) > &0 {  // assign clergy
                                        available_workers.hashmap.insert(WorkType::Clergy, available_workers.hashmap.get(&WorkType::Clergy).unwrap_or(&0) - 1);
                                        workers_needed -= 1;
                                    }
                                    // NOTE: Clergy is a specialist job and won't take unemployed pops for now.
                                }
                                // if all workers are successfully assigned, job is done
                                self.labor_manifest.push(request.unique_id());
                            } else {                // not enough available workers
                                // do nothing for now
                            }
                        },
                        _ => {},
                    }
                },
                None => {
                    println!("You shouldn't be here! Got None from Job Request!")
                }
            }
        }

        if LABOR_MANIFEST_DEBUG { // for DEBUGGING purposes only
            println!("LABOR MANIFEST: {:?}", self.labor_manifest);
        }
    }

    // Now, we need a function where each building checks to see if its work order has been fulfilled
    // in the LaborManifest and if so, produce it's selected resources or outputs.
    pub fn produce_from_labor_manifest(&mut self) {
        let mut results = Vec::<BuildingProductionResult>::new();

        for building in &mut self.buildings {
            if building.expected_manifest_ids() > 1 {
                let mut count = 0;
                let expected = building.expected_manifest_ids();


                while count < expected {
                    match self.labor_manifest.iter().position(|a| a == &building.work_id()) {
                        Some(i) => {
                            if PRODUCTION_FROM_MANIFEST_DEBUG {
                                println!("[PRODUCE_FROM_LABOR_MANIFEST] Labor id {} found at {}. Need {} more.", building.work_id(), i, expected - (count+1));
                            }
                            self.labor_manifest.remove(i);
                            count += 1;
                        },
                        None => {
                            if PRODUCTION_FROM_MANIFEST_DEBUG {
                                println!("[PRODUCE_FROM_LABOR_MANIFEST] Labor id {} not found. Breaking...", building.work_id());
                            }
                            break;
                        }
                    }
                }

                if count == expected {
                    if PRODUCTION_FROM_MANIFEST_DEBUG {
                        println!("[PRODUCE_FROM_LABOR_MANIFEST] Producing for {}.", building.work_id());
                    }
                    // let result = match building.get_data_tags().resource_needs.len() {
                    //     0 => {
                    //         building.produce()
                    //     }
                    //     _ => {
                    //         for need in building.get_data_tags().resource_needs.clone() {
                    //             // Currently this doesn't check if all needs are met before consuming resources. It's hungry.
                    //             // self.get_mut_stockpile().withdraw(Resource::Grains(_), 5.0);
                    //         }
                    //         building.produce()
                    //     }
                    // };
                    let result = building.produce();
                    results.push(result);
                } else {
                    building.dont_produce();
                }
            } else {
                if self.labor_manifest.contains(&building.work_id()) {
                    if PRODUCTION_FROM_MANIFEST_DEBUG {
                        println!("[PRODUCE_FROM_LABOR_MANIFEST] Producing for {}.", building.work_id());
                    }
                    // let result = match building.get_data_tags().resource_needs.len() {
                    //     0 => {
                    //         building.produce()
                    //     }
                    //     _ => {
                    //         for need in building.get_data_tags().resource_needs.clone() {

                    //         }
                    //         building.produce()
                    //     }
                    // };
                    let result = building.produce();
                    results.push(result);
                } else {
                    building.dont_produce();
                }
            }
        }

        for result in results {
            self.handle_production_result(result);
        }

        self.labor_manifest.clear();
    }

    pub fn handle_production_result(&mut self, result: BuildingProductionResult) {
        match result.bundle {
            Some(bundle) => {
                self.stockpile.insert(bundle);
            },
            None => {}
        }
        match result.bundle_collection {
            Some(collection) => {
                for bundle in collection {
                    self.stockpile.insert(bundle);
                }
            },
            None => {}
        }
    }

    pub fn get_mut_stockpile(&mut self) -> &mut Stockpile {
        return &mut self.stockpile;
    }

    pub fn get_stockpile(&self) -> &Stockpile {
        return &self.stockpile;
    }
}
