// use std::collections::*;

use crate::civilization::labor::*;

#[derive(Clone)]
pub struct Population {
    size: u64,
    workers: Workers,
    // groups: Vec<PopulationGroup>
}

impl Population {
    pub fn new(size: u64) -> Population {
        let mut new_pop = Population {
            size,
            workers: Workers::new(),
        };
        new_pop.workers.hashmap.insert(WorkType::Unemployed, size);
        return new_pop;
    }

    pub fn grow_by(&mut self, growth: u64) -> u64 {
        self.size += growth;

        let previous = self.workers.hashmap.get(&WorkType::Unemployed).unwrap_or(&0);
        self.workers.hashmap.insert(WorkType::Unemployed, growth);

        return self.size;
    }

    pub fn size(&self) -> u64 {
        return self.size;
    }

    pub fn get_workers(&mut self) -> Workers {
        return self.workers.duplicate();
    }
}

// considering managing individual people with this, but i'm afraid of the effects that might
// have on performance and what i can do as things start to get bigger...
pub struct Person {
    name: String,
    job: WorkType,
}