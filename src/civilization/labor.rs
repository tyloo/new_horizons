use std::collections::HashMap;

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum WorkType {
    Unemployed,
    LocalWelfare,
    NationalWelfare,

    Youth,
    Retired,

    BasicConstruction,

    Domestic,
    Medical,
    Librarian,
    Clergy,
    Education,

    Mayor,
    Clerk,

    Forestry,
    Farming,
    Shepherd,
    Hunting,

    LightIndustry,
    HeavyIndustry,
    Processing,
}

impl WorkType {
    // determines if a worker currently working "self" is qualified to work the argument work type
    // this is a pretty rought implementation... and i'm trying to think of a better way to do it...
    // it will probably mean dividing population by "education level" or something of that sort
    // and then judging their qualifications based on that, but that will take some time
    pub fn qualified_for(&self, target_work: WorkType) -> bool {
        match self {
            Unemployed => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
            LocalWelfare => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
            NationalWelfare => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
        
            Youth => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
            Retired => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
        
            BasicConstruction => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
        
            Domestic => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
            Medical => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
            Librarian => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
            Clergy => {
                match target_work {
                    _ => return true,
                }
            },
        
            Forestry => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
            Farming => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
            Herding => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
        
            LightIndustry => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
            HeavyIndustry => {
                match target_work {
                    Clergy => return false,
                    _ => return true,
                }
            },
            _ => return false,
        }
    }

    // work in progress. Returns a vector of all the work types that self can also work. 
    // this is probably a better implementation that qualified_for()
    pub fn qualifications(&self) -> Vec<WorkType> {
        match self {
            _ => return vec!(WorkType::Unemployed)
        }
    }
}


#[allow(non_snake_case)]
#[derive(Clone, Eq, PartialEq)]
pub struct Workers {
    pub hashmap: HashMap<WorkType, u64>,
}

impl Workers {
    pub fn new() -> Workers {
        let new_workers = Workers {
            hashmap: HashMap::new(),
        };
        return new_workers;
    }

    pub fn duplicate(&mut self) -> Workers {
        return self.clone();
    }
}

// Keeps track of jobs and requests.
#[derive(Clone, Debug)]
pub struct JobRegistry {
    work_queue: Vec<LaborRequest>,
}

impl JobRegistry {
    pub fn new() -> JobRegistry {
        let registry = JobRegistry {
            work_queue: Vec::new(),
        };
        return registry;
    }

    pub fn empty(&mut self) {
        self.work_queue.clear();
    }

    pub fn add_request(&mut self, request: LaborRequest) {
        self.work_queue.push(request);
    }

    pub fn pop_request(&mut self) -> Option<LaborRequest> {
        return self.work_queue.pop();
    }

    pub fn is_empty(&mut self) -> bool {
        return self.work_queue.is_empty();
    }

    pub fn priority_last_sort(&mut self) {
        // sorts the JobRequests BACKWARDS by their priority, putting highest priority jobs last
        // this is because Vec is a stack, not a queue, and the last job will be pop() first
        // and by "highest priority" I actually mean the jobs with the LOWEST number
        self.work_queue.sort_by(|b, a| a.priority().cmp(&b.priority()));
    }
}

// Contains a work type and a number of workers needed to fulfill the request.
#[derive(Clone, Debug)]
pub struct LaborRequest {
    id: String,
    priority: u64,
    work_type: WorkType,
    workers: u64,
}

impl LaborRequest {
    pub fn new(id: String, priority: u64, work_type: WorkType, workers: u64,) -> LaborRequest {
        let request = LaborRequest {
            id,
            priority,
            work_type,
            workers,
        };
        return request;
    }

    pub fn unique_id(&self) -> String {
        return self.id.clone();
    }

    pub fn priority(&self) -> u64 {
        return self.priority;
    }

    pub fn work_type(&self) -> WorkType {
        return self.work_type.clone();
    }

    pub fn workers_needed(&self) -> u64 {
        return self.workers;
    }
}