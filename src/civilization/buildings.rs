use crate::civilization::labor::*;
use crate::civilization::settlement::*;

use crate::world::resources::*;

// Job priority numbers. Lower number means higher priority!
// Might be relocated later. 
// Players will be able to customize these later.
const CONSTRUCTION_PRIORITY: u64 = 30;
const FARMING_PRIORITY: u64 = 10;
const WOODCUTTER_PRIORITY: u64 = 11;
const MILLER_PRIORITY: u64 = 30;
const LUMBERJACK_PRIORITY: u64 = 20;
const DOMESTIC_PRIORITY: u64 = 15;
const LIBRARIAN_PRIORITY: u64 = 41;
const MEDICAL_PRIORITY: u64 = 1;
const CLERGY_PRIORITY: u64 = 45;
const HOMESTEAD_PRIORITY: u64 = 9;
const PASTURE_PRIORITY: u64 = 12;
const HUNTER_PRIORITY: u64 = 13;
const BUTCHER_PRIORITY: u64 = 31;
const RANGER_PRIORITY: u64 = 25;
const WINDMILL_PRIORITY: u64 = 25;
const TEACHER_PRIORITY: u64 = 40;
const TOWNHALL_PRIORITY: u64 = 25;

const SUPPRESS_PRODUCTION_HELPER_WARNINGS: bool = true;

#[derive(Clone, Debug)]
pub enum BuildingType {
    Empty,

    // housing
    SimpleHouse,
    Homestead,

    // infrastructure
    Stockpile,
    PrimitiveSewer,

    // land developments
    Farmland,
    WoodcuttersCamp,
    Pasture,

    // tier1 buildings
    Lumbermill,
    Gristmill,
    HuntersLodge,
    Butcher,

    // tier2 buildings
    RangersTower,
    Granary,
    Windmill,

    // tier3 buildings
    Library,
    Clinic,
    Chapel,
    Schoolhouse,
    
    TownHall,
}

#[derive(Clone)]
enum BuildingLevel {
    Basic,
}

#[derive(Clone)]
enum ConstructionState {
    Complete,
    InProgress(u64, u64), // amount of Construction required, number of construction jobs available
    Repairing(u64, u64),
    // UpgradingToLevel((u64, u64)),
}

#[derive(Clone, Debug)]
pub enum FarmProductionState {
    Empty,
    Growing(u16, Resource),
    Ready(Resource),
}

#[derive(Clone, Debug)]
pub struct BuildingDataTags {
    pub farm_production_state: FarmProductionState,
    pub farm_crop_type: Option<Resource>,

    pub resource_needs: Vec<(Resource, f64)>,
}

impl BuildingDataTags {
    pub fn new() -> BuildingDataTags {
        return BuildingDataTags {
            farm_production_state: FarmProductionState::Empty,
            farm_crop_type: None,

            resource_needs: Vec::new(),
        }
    }
}

#[derive(Clone)]
pub struct SettlementBuilding {
    name: String,
    work_id: String,
    expected_ids: u8,
    construction_status: ConstructionState,
    building_type: BuildingType,
    level: BuildingLevel,
    land_use: f64,
    forest_land_requirement: f64,
    arable_land_requirement: f64,
    minimum_fertitlity_requirement: f64,
    jobs: Vec<LaborRequest>,
    produced_last_cycle: bool,
    data_tags: BuildingDataTags,
}

impl SettlementBuilding {
    pub fn work_id(&self) -> String {
        return self.work_id.clone();
    }

    pub fn expected_manifest_ids(&self) -> u8 {
        return self.expected_ids;
    }

    pub fn building_type(&self) -> &BuildingType {
        return &self.building_type;
    }

    pub fn get_jobs(&self) -> Vec<LaborRequest> {
        let work_id = self.work_id.clone();

        match &self.construction_status {
            ConstructionState::Complete => {
                return self.jobs.clone();
            },
            ConstructionState::InProgress(_, workers) => {
                return vec!(LaborRequest::new(work_id, CONSTRUCTION_PRIORITY, WorkType::BasicConstruction, *workers));
            },
            ConstructionState::Repairing(_, workers) => {
                return vec!(LaborRequest::new(work_id, 5, WorkType::BasicConstruction, *workers));
            }
        }
    }

    pub fn get_data_tags(&self) -> &BuildingDataTags {
        return &self.data_tags;
    }

    // a debugging function which sets the state of the building to ConstructionState::Complete
    // actual construction progress will be handled by a separate function
    pub fn debug_complete_construction(&mut self) {
        self.construction_status = ConstructionState::Complete;
    }

    // A core building function. Performs the building's output actions depending on the building's type and level.
    pub fn produce(&mut self) -> BuildingProductionResult {
        match self.construction_status {
            ConstructionState::Complete => {
                let result = self.production_helper();
                self.produced_last_cycle = true;
                return result;
            },
            ConstructionState::InProgress(construction_needed, builders) => {
                if construction_needed <= builders  {
                    self.construction_status = ConstructionState::Complete;
                    // Move this bit of code here because otherwise every building with multiple jobs when completed
                    // was expecting to have an equal number of builder jobs when actually it should just have one
                    // builder job.
                    self.expected_ids = self.jobs.len() as u8;
                    return BuildingProductionResult::new(None, None);
                } else {
                    self.construction_status = ConstructionState::InProgress(construction_needed - builders, builders);
                    return BuildingProductionResult::new(None, None);
                }
            },
            ConstructionState::Repairing(construction_needed, builders) => {
                if builders <= construction_needed {
                    self.construction_status = ConstructionState::Complete;
                    return BuildingProductionResult::new(None, None);
                } else {
                    self.construction_status = ConstructionState::Repairing(construction_needed - builders, builders);
                    return BuildingProductionResult::new(None, None);
                }
            },
        }
    }

    fn production_helper(&mut self) -> BuildingProductionResult {
        match self.building_type {
            BuildingType::Homestead => {
                match &self.data_tags.farm_production_state {
                    FarmProductionState::Empty => {
                        self.data_tags.farm_production_state = FarmProductionState::Growing(
                            1, 
                            self.data_tags.farm_crop_type.clone().unwrap_or(Resource::Grains(GrainType::Oats))
                        );
                        // currently, if farmlands are not given a specific crop to grow, they will grow oats
                        return BuildingProductionResult::new(None, Some(vec!(
                            Bundle::new(Resource::Wood, 2.0), 
                            Bundle::new(Resource::Wool, 1.0),
                            Bundle::new(Resource::Milk, 1.0)
                        )));
                    },
                    FarmProductionState::Growing(state, crop) => {
                        // currently all crops need 10 turns to grow, then produce 10 times the amount of food they used to produce per turn
                        // crops are harvest on the 11th turn! then replanted at state Growing(1)
                        if *state < 9 {
                            // IMPORTANT: This needs to be 1 less than the number of turns needed to grow the crop.
                            // Because on the Nth turn, the state should transition to Ready() instead of Growing(N).
                            self.data_tags.farm_production_state = FarmProductionState::Growing(state+1, crop.clone());
                            return BuildingProductionResult::new(None, Some(vec!(
                                Bundle::new(Resource::Wood, 2.0), 
                                Bundle::new(Resource::Wool, 1.0),
                                Bundle::new(Resource::Milk, 1.0)
                            )));
                        } else {
                            self.data_tags.farm_production_state = FarmProductionState::Ready(crop.clone());
                            return BuildingProductionResult::new(None, Some(vec!(
                                Bundle::new(Resource::Wood, 2.0), 
                                Bundle::new(Resource::Wool, 1.0),
                                Bundle::new(Resource::Milk, 1.0)
                            )));
                        }
                    },
                    FarmProductionState::Ready(crop) => {
                        let product_crop = crop.clone(); // needed to avoid borrowchecker problems on the lines below
                        self.data_tags.farm_production_state = FarmProductionState::Growing(
                            1, 
                            self.data_tags.farm_crop_type.clone().unwrap_or(Resource::Grains(GrainType::Oats))
                        );
                        return BuildingProductionResult::new(None, Some(vec!(
                            Bundle::new(product_crop, 60.0),
                            Bundle::new(Resource::Wood, 2.0), 
                            Bundle::new(Resource::Wool, 1.0),
                            Bundle::new(Resource::Milk, 1.0)
                        )));
                    },
                }
            }
            BuildingType::Farmland => {
                match &self.data_tags.farm_production_state {
                    FarmProductionState::Empty => {
                        self.data_tags.farm_production_state = FarmProductionState::Growing(
                            1, 
                            self.data_tags.farm_crop_type.clone().unwrap_or(Resource::Grains(GrainType::Oats))
                        );
                        // currently, if farmlands are not given a specific crop to grow, they will grow oats
                        return BuildingProductionResult::new(None, None);
                    },
                    FarmProductionState::Growing(state, crop) => {
                        // currently all crops need 10 turns to grow, then produce 10 times the amount of food they used to produce per turn
                        // crops are harvest on the 11th turn! then replanted at state Growing(1)
                        if *state < 9 {
                            // IMPORTANT: This needs to be 1 less than the number of turns needed to grow the crop.
                            // Because on the Nth turn, the state should transition to Ready() instead of Growing(N).
                            self.data_tags.farm_production_state = FarmProductionState::Growing(state+1, crop.clone());
                            return BuildingProductionResult::new(None, None);
                        } else {
                            self.data_tags.farm_production_state = FarmProductionState::Ready(crop.clone());
                            return BuildingProductionResult::new(None, None);
                        }
                    },
                    FarmProductionState::Ready(crop) => {
                        let product_crop = crop.clone(); // needed to avoid borrowchecker problems on the lines below
                        self.data_tags.farm_production_state = FarmProductionState::Growing(
                            1, 
                            self.data_tags.farm_crop_type.clone().unwrap_or(Resource::Grains(GrainType::Oats))
                        );
                        return BuildingProductionResult::new(Some(Bundle::new(product_crop, 100.0)), None);
                    },
                }
            }
            BuildingType::WoodcuttersCamp => {
                return BuildingProductionResult::new(Some(Bundle::new(Resource::Wood, 10.0)), None);
            }
            _ => {
                if !SUPPRESS_PRODUCTION_HELPER_WARNINGS {
                    println!("Building type {:?} does not have a branch defined in production_helper()!", self.building_type);
                }
                return BuildingProductionResult::new(None, None);
            }
        }
    }

    // called to set produced_last_cycle to false
    // also helps to manage capacities and settlement characteristics
    // might need a better name :/
    pub fn dont_produce(&mut self) {
        self.produced_last_cycle = false;
    }



    //          --- B U I L D I N G   D E F I N I T I O N S ---
    pub fn new_simple_house(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            // TODO: ADDS 6 HOUSING!!!
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(10, 2),
            building_type: BuildingType::SimpleHouse,
            level: BuildingLevel::Basic,
            land_use: 0.01,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, DOMESTIC_PRIORITY, WorkType::Domestic, 1)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_homestead(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            // TODO: ADDS 6 HOUSING!!!
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(12, 6),
            building_type: BuildingType::Homestead,
            level: BuildingLevel::Basic,
            land_use: 0.25,
            forest_land_requirement: 0.5,
            arable_land_requirement: 0.25,
            minimum_fertitlity_requirement: 0.25,
            jobs: vec!(
                LaborRequest::new(new_id.clone(), HOMESTEAD_PRIORITY, WorkType::Domestic, 1),
                LaborRequest::new(new_id.clone(), HOMESTEAD_PRIORITY, WorkType::Farming, 3),
                LaborRequest::new(new_id.clone(), HOMESTEAD_PRIORITY, WorkType::Forestry, 1),
                LaborRequest::new(new_id        , HOMESTEAD_PRIORITY, WorkType::Shepherd, 1),
            ),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_farmland(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(30, 5),
            building_type: BuildingType::Farmland,
            level: BuildingLevel::Basic,
            land_use: 0.5,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.5,
            minimum_fertitlity_requirement: 0.25,
            jobs: vec!(LaborRequest::new(new_id, FARMING_PRIORITY, WorkType::Farming, 5)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_woodcutter(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(30, 5),
            building_type: BuildingType::WoodcuttersCamp,
            level: BuildingLevel::Basic,
            land_use: 0.1,
            forest_land_requirement: 2.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, WOODCUTTER_PRIORITY, WorkType::Forestry, 5)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_pasture(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(30, 5),
            building_type: BuildingType::Pasture,
            level: BuildingLevel::Basic,
            land_use: 0.5,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.25,
            jobs: vec!(LaborRequest::new(new_id, PASTURE_PRIORITY, WorkType::Shepherd, 5)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_gristmill(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(30, 5),
            building_type: BuildingType::Gristmill,
            level: BuildingLevel::Basic,
            land_use: 0.01,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, MILLER_PRIORITY, WorkType::Processing, 5)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_lumbermill(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(30, 5),
            building_type: BuildingType::Lumbermill,
            level: BuildingLevel::Basic,
            land_use: 0.01,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, LUMBERJACK_PRIORITY, WorkType::LightIndustry, 5)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_hunters_lodge(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(30, 5),
            building_type: BuildingType::HuntersLodge,
            level: BuildingLevel::Basic,
            land_use: 0.02,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, HUNTER_PRIORITY, WorkType::Hunting, 5)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_butcher(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(30, 5),
            building_type: BuildingType::Butcher,
            level: BuildingLevel::Basic,
            land_use: 0.005,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, BUTCHER_PRIORITY, WorkType::Processing, 5)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_rangers_tower(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(30, 5),
            building_type: BuildingType::RangersTower,
            level: BuildingLevel::Basic,
            land_use: 0.02,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, RANGER_PRIORITY, WorkType::Forestry, 5)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_windmill(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(50, 5),
            building_type: BuildingType::Windmill,
            level: BuildingLevel::Basic,
            land_use: 0.005,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, WINDMILL_PRIORITY, WorkType::BasicConstruction, 1)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_library(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(45, 5),
            building_type: BuildingType::Library,
            level: BuildingLevel::Basic,
            land_use: 0.05,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, LIBRARIAN_PRIORITY, WorkType::Librarian, 4)),
            // libraries should be able to store books and gain a bonus from the number of books stored.
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_clinic(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(45, 5),
            building_type: BuildingType::Clinic,
            level: BuildingLevel::Basic,
            land_use: 0.02,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, MEDICAL_PRIORITY, WorkType::Medical, 4)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_chapel(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(30, 5),
            building_type: BuildingType::Chapel,
            level: BuildingLevel::Basic,
            land_use: 0.01,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, CLERGY_PRIORITY, WorkType::Clergy, 2)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_school_house(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(30, 5),
            building_type: BuildingType::Schoolhouse,
            level: BuildingLevel::Basic,
            land_use: 0.02,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(LaborRequest::new(new_id, TEACHER_PRIORITY, WorkType::Education, 5)),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }

    pub fn new_town_hall(new_name: String, new_id: String) -> SettlementBuilding {
        let mut building = SettlementBuilding {
            name: new_name,
            work_id: new_id.clone(),
            expected_ids: 0,
            construction_status: ConstructionState::InProgress(100, 10),
            building_type: BuildingType::TownHall,
            level: BuildingLevel::Basic,
            land_use: 0.05,
            forest_land_requirement: 0.0,
            arable_land_requirement: 0.0,
            minimum_fertitlity_requirement: 0.0,
            jobs: vec!(
                LaborRequest::new(new_id.clone(), TOWNHALL_PRIORITY, WorkType::Mayor, 1),
                LaborRequest::new(new_id        , TOWNHALL_PRIORITY, WorkType::Clerk, 4),
            ),
            produced_last_cycle: false,
            data_tags: BuildingDataTags::new(),
        };
        return building;
    }
}

// Contains a set of Options which represent all possible non-functional outputs of a building's production.
// Might also include tags which are passed back up to the settlement level and then executed upon.
pub struct BuildingProductionResult {
    pub bundle: Option<Bundle>,
    pub bundle_collection: Option<Vec<Bundle>>,
}

impl BuildingProductionResult {
    pub fn new(bundle: Option<Bundle>, bundle_collection: Option<Vec<Bundle>>) -> BuildingProductionResult {
        let result = BuildingProductionResult {
            bundle,
            bundle_collection,
        };
        return result;
    }
}
