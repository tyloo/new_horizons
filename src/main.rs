extern crate ggez;

mod civilization;
mod engine;
mod utils;
mod world;

use crate::civilization::buildings::*;
use crate::civilization::character::*;
use crate::civilization::entity::*;
use crate::civilization::settlement::*;
use crate::civilization::unit::*;

use engine::engine::*;

use utils::tests::*;

use world::coordinate::*;
use world::resources::*;
use world::world::*;

use std::{thread, time};
use std::time::{Instant};

use ggez::*;
use rand::*;

fn main() {
    let start = Instant::now();
    println!("[MAIN] Hello, world!");

    let mut world: World = World::new_with_radius(10);
    println!("[MAIN] Finished world generation {} milliseconds after start.", start.elapsed().as_millis());

    let mut entities: EntityList = EntityList::new();
    println!("[MAIN] Initialized entity list {} milliseconds after start.", start.elapsed().as_millis());

    // Run either main_test() or demo().
    let run_tests = false;
    // Do not run both. Unexpected errors may occur.
    if run_tests { 
        main_test(&mut world, &mut entities, start); 
    } else {
        demo(&mut world, &mut entities);
    }


    // This is a very messy way to load a ggez configuration.
    // TODO: Load Conf from a .toml file using ggez::conf::Conf::from_toml_file().
    let c = conf::Conf {
        window_mode: ggez::conf::WindowMode {
            width: 1000.0,
            height: 800.0,
            maximized: false,
            fullscreen_type: ggez::conf::FullscreenType::Windowed,
            borderless: false,
            min_width: 400.0,
            max_width: 3840.0,
            min_height: 300.0,
            max_height: 2160.0,
            resizable: false, // until i can figure out why resizing breaks the scaling...
        },
        window_setup: ggez::conf::WindowSetup {
            title: "New Horizons | ggez".to_owned(),
            samples: ggez::conf::NumSamples::Zero,
            vsync: true,
            icon: "".to_owned(),
            srgb: true,
        },
        backend: ggez::conf::Backend::default(),
        modules: ggez::conf::ModuleConf::default(),
    };
    let (ref mut ctx, ref mut event_loop) = ContextBuilder::new("New Horizons", "Tyler H.")
        .conf(c)
        .build()
        .unwrap();
    let mut engine = GameEngine::new(world, entities, ctx);
    
    event::run(ctx, event_loop, &mut engine).unwrap();
}

// Initializes a settlement and unit for demo purposes.
fn demo(world: &mut World, entities: &mut EntityList) {
    let location = match find_viable_settle_location_helper(world) {
        Some(coordinate) => coordinate,
        None => {
            println!("   Settle location helper returned None.");
            println!("   Skipping the rest of main_test()...!");
            return;
        }
    };

    //      --- S E T T L E M E N T ---
    // Creates a Settlement.
    world.add_settlement_at(location, Settlement::new(String::from("Lestallum"), SettlementLevel::Village, location.0, location.1, location.2, 200));

    // Adds some buildings to the settlement.
    for i in 0..20 {
        world.get_mut_settlement_at(location)
            .unwrap()
            .add_building(SettlementBuilding::new_homestead(format!("new homestead {}", i), format!("homestead{}", i)));
    }
    for i in 0..10 {
        world.get_mut_settlement_at(location)
            .unwrap()
            .add_building(SettlementBuilding::new_woodcutter(format!("new woodcutter {}", i), format!("woodcutter{}", i)));
    }
    for i in 0..5 {
        world.get_mut_settlement_at(location)
            .unwrap()
            .add_building(SettlementBuilding::new_gristmill(format!("new gristmill {}", i), format!("gristmill{}", i)));
        world.get_mut_settlement_at(location)
            .unwrap()
            .get_mut_stockpile().insert(Bundle::new(Resource::Grains(GrainType::Oats), 150.0));
    }

    // let crown_location: (i32, i32, i32) = (0, 0, 0);
    // world.add_settlement_at(crown_location, Settlement::new(String::from("Crown"), SettlementLevel::Outpost, crown_location.0, crown_location.1, crown_location.2, 25));

    // // Adds buildings to the settlement.
    // for i in 0..4 {
    //     world.get_mut_settlement_at(crown_location)
    //         .unwrap()
    //         .add_building(SettlementBuilding::new_farmland(format!("new farmland {}", i), format!("farmland{}", i)));
    // }
    // world.get_mut_settlement_at(crown_location)
    //         .unwrap()
    //         .add_building(SettlementBuilding::new_library(format!("crown library"), format!("crownlibrary")));

    // world.get_mut_settlement_at(crown_location)
    //     .unwrap()
    //     .get_mut_stockpile().insert(Bundle::new(Resource::Grains(GrainType::Oats), 150.0));


    //      --- C H A R A C T E R   A N D   E N T I T Y ---
    let mut protagonist = Character::new(String::from("Oyuunni Astraea"));
    let mut protag_entity = Entity::new_at(String::from("First Trailblazers"), String::from("FirstTrailblazers"), location.0, location.1, location.2);
    
    let (alias, index) = entities.add_entity(protag_entity);
    
    entities.get_mut_entity_with_alias(alias.clone()).add_unit(UnitGroup::Individual(protagonist));

    let mut rng = thread_rng();
    let result = match rng.gen_range(0, 6) {
        0 => {
            entities.move_aliased_entity(world, alias.clone(), Direction::East)
        },
        1 => {
            entities.move_aliased_entity(world, alias.clone(), Direction::West)
        },
        2 => {
            entities.move_aliased_entity(world, alias.clone(), Direction::Northwest)
        },
        3 => {
            entities.move_aliased_entity(world, alias.clone(), Direction::Southeast)
        },
        4 => {
            entities.move_aliased_entity(world, alias.clone(), Direction::Southwest)
        },
        5 => {
            entities.move_aliased_entity(world, alias.clone(), Direction::Northeast)
        },
        _ => {
            // Match should never reach here, but rand is strange and therefore rust needs this.
            return ();
        }
    };

}

// Gameplay cycle.
fn tick() { //better name needed
    // learn to take input
}

// Upkeep and update cycle.
fn tock() { //better name needed

}