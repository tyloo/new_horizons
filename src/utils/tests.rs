use crate::civilization::buildings::*;
use crate::civilization::character::*;
use crate::civilization::entity::*;
use crate::civilization::settlement::*;
use crate::civilization::unit::*;

use crate::utils::shuffle_and_sort::*;

use crate::world::coordinate::*;
use crate::world::world::*;

use std::time::{Instant};

use rand::*;

pub fn main_test(world: &mut World, entities: &mut EntityList, start: Instant) {
    let location = match find_viable_settle_location_helper(world) {
        Some(coordinate) => coordinate,
        None => {
            println!("   Settle location helper returned None.");
            println!("   Skipping the rest of main_test()...!");
            return;
        }
    };

    settlement_test(world, location, start);
    character_and_entity_test(world, entities, location, start);
}

pub fn find_viable_settle_location_helper(world: &mut World) -> Option<(i32, i32, i32)> {
    let mut coordinate_set = world.get_internal_set_clone();
    shuffle(&mut coordinate_set);
    
    let mut location = match coordinate_set.pop_front() {
        Some(coordinate) => coordinate,
        None => {
            println!("NOW IS A GOOD TIME TO PANIC!");
            println!("   The function coordinate_set.pop_front() returned None during the first call.");
            return None;
        }
    };
    let mut viable_location_found = false;

    // This while loop checks individual coordinates in a random order for
    // a tile that has biome characteristics that meet certain requirements. 
    // If it finds one, the while loop breaks, if it doesn't find one
    // it will skip the rest of main_test().
    while !viable_location_found {
        // Using regular unwraps here because there shouldn't be any locations in the VecDeque that aren't initialized. 
        // This is just test code, so I hope that's going to be okay.
        if world.get_tile_at(location).unwrap().get_biome_characteristics().available_land() >= 50.0 
            && world.get_tile_at(location).unwrap().get_biome_characteristics().forest_coverage() >= 20.0 
            && world.get_tile_at(location).unwrap().get_biome_characteristics().arable_land_coverage() >= 20.0 
            && world.get_tile_at(location).unwrap().get_biome_characteristics().fertility_rating() >= 0.25 {
                viable_location_found = true;
        } else {
            match coordinate_set.pop_front() {
                Some(coordinate) => {
                    location = coordinate;
                },
                None => {
                    println!("Coordinate set is now empty. No viable location found.");
                    return None;
                }
            }
        }
    }

    return Some(location);
}

fn settlement_test(world: &mut World, location: (i32, i32, i32), start: Instant) {
    println!();
    world.add_settlement_at(location, Settlement::new(String::from("Lestallum"), SettlementLevel::Village, location.0, location.1, location.2, 600));
    let tile = world.get_tile_at(location).unwrap();
    // Another sketchy unwrap. This location has to be initialized to get this far in the code, so...
    println!("[SETTLEMENT] Created Lestallum at {:?} {} milliseconds after start.", location, start.elapsed().as_millis());
    println!("[SETTLEMENT]   Lestallum is located in a {:?} biome.", tile.get_biome());
    println!("[SETTLEMENT]   The region has the following features: {:?}.", tile.get_biome_features());
    println!("[SETTLEMENT]   The region has {:?}% available land.", tile.get_biome_characteristics().available_land());
    println!("[SETTLEMENT]   The region has {:?}% forest coverage.", tile.get_biome_characteristics().forest_coverage());
    println!("[SETTLEMENT]   The region has {:?}% arable land coverage.", tile.get_biome_characteristics().arable_land_coverage());
    println!("[SETTLEMENT]   The region has {:?}% fertility rating.", tile.get_biome_characteristics().fertility_rating()*100.0);
    println!("[SETTLEMENT]   The region is located at an altitude of {:?}.", tile.get_biome_characteristics().altitude());
    println!("[SETTLEMENT]   The region has an average temperature of {:?}.", tile.get_biome_characteristics().temperature());

    let l = world.get_mut_settlement_at(location).unwrap();
    // Using a regular unwrap here because I don't think this should ever return None for this particular case.

    // HOMESTEADS
    for i in 0..5 {
        l.add_building(SettlementBuilding::new_homestead(format!("new homestead {}", i), format!("homestead{}", i)));
    }
    println!("[SETTLEMENT] Added 5 Homesteads {} milliseconds after start.", start.elapsed().as_millis());
    // FARMLANDS
    for i in 0..10 {
        l.add_building(SettlementBuilding::new_farmland(format!("new farm {}", i), format!("farm{}", i)));
    }
    println!("[SETTLEMENT] Added 10 Farmlands {} milliseconds after start.", start.elapsed().as_millis());
    // WOODCUTTERS
    for i in 0..10 {
        l.add_building(SettlementBuilding::new_woodcutter(format!("new woodcutter {}", i), format!("woodcutter{}", i)));
    }
    println!("[SETTLEMENT] Added 10 Woodcutters {} milliseconds after start.", start.elapsed().as_millis());
    // PASTURES
    for i in 0..10 {
        l.add_building(SettlementBuilding::new_pasture(format!("new pasture {}", i), format!("pasture{}", i)));
    }
    println!("[SETTLEMENT] Added 10 Pastures {} milliseconds after start.", start.elapsed().as_millis());
    // GRISTMILLS
    for i in 0..2 {
        l.add_building(SettlementBuilding::new_gristmill(format!("new gristmill {}", i), format!("gristmill{}", i)));
    }
    println!("[SETTLEMENT] Added 2 Gristmills {} milliseconds after start.", start.elapsed().as_millis());
    // LUMBERMILLS
    for i in 0..2 {
        l.add_building(SettlementBuilding::new_lumbermill(format!("new lumbermill {}", i), format!("lumbermill{}", i)));
    }
    println!("[SETTLEMENT] Added 2 Lumbermills {} milliseconds after start.", start.elapsed().as_millis());
    // HUNTERS'S LODGE
    for i in 0..5 {
        l.add_building(SettlementBuilding::new_hunters_lodge(format!("new hunters_lodge {}", i), format!("hunters_lodge{}", i)));
    }
    println!("[SETTLEMENT] Added 5 Hunters' Lodges {} milliseconds after start.", start.elapsed().as_millis());
    // BUTCHERS
    for i in 0..2 {
        l.add_building(SettlementBuilding::new_butcher(format!("new butcher {}", i), format!("butcher{}", i)));
    }
    println!("[SETTLEMENT] Added 2 Butchers {} milliseconds after start.", start.elapsed().as_millis());
    // SIMPLE HOUSES
    for i in 0..95 {
        l.add_building(SettlementBuilding::new_simple_house(format!("new house {}", i), format!("house{}", i)));
    }
    println!("[SETTLEMENT] Added 95 Simple Houses {} milliseconds after start.", start.elapsed().as_millis());

    // RANGERS' TOWER
    for i in 0..1 {
        l.add_building(SettlementBuilding::new_rangers_tower(format!("new ranger's tower {}", i), format!("rangers_tower{}", i)));
    }
    println!("[SETTLEMENT] Added 1 Rangers' Tower {} milliseconds after start.", start.elapsed().as_millis());
    // WINDMILL
    for i in 0..1 {
        l.add_building(SettlementBuilding::new_windmill(format!("new windmill {}", i), format!("windmill{}", i)));
    }
    println!("[SETTLEMENT] Added 1 Windmill {} milliseconds after start.", start.elapsed().as_millis());
    // LIBRARY
    for i in 0..1 {
        l.add_building(SettlementBuilding::new_library(format!("new library {}", i), format!("library{}", i)));
    }
    println!("[SETTLEMENT] Added 1 Library {} milliseconds after start.", start.elapsed().as_millis());
    // CLINIC
    for i in 0..1 {
        l.add_building(SettlementBuilding::new_clinic(format!("new clinic {}", i), format!("clinic{}", i)));
    }
    println!("[SETTLEMENT] Added 1 Clinic {} milliseconds after start.", start.elapsed().as_millis());
    // CHAPEL
    for i in 0..1 {
        l.add_building(SettlementBuilding::new_chapel(format!("new chapel {}", i), format!("chapel{}", i)));
    }
    println!("[SETTLEMENT] Added 1 Chapel {} milliseconds after start.", start.elapsed().as_millis());
    // SCHOOLHOUSE
    for i in 0..1 {
        l.add_building(SettlementBuilding::new_school_house(format!("new schoolhouse {}", i), format!("school_house{}", i)));
    }
    println!("[SETTLEMENT] Added 1 Schoolhouse {} milliseconds after start.", start.elapsed().as_millis());
    // CHAPEL
    for i in 0..1 {
        l.add_building(SettlementBuilding::new_town_hall(format!("new town hall {}", i), format!("town_hall{}", i)));
    }
    println!("[SETTLEMENT] Added 1 Town Hall {} milliseconds after start.", start.elapsed().as_millis());

    let mut count = 0; // just want to make sure it does every building
    // for building in l.buildings_mut() {
    //     building.debug_complete_construction();
    //     count += 1;
    // }
    println!("[SETTLEMENT] Force completed {} buidlings {} milliseconds after start.", count, start.elapsed().as_millis());

    l.refresh_job_list();
    println!("[SETTLEMENT] Refreshed and sorted the job list {} milliseconds after start.", start.elapsed().as_millis());
    l.distribute_labor();
    println!("[SETTLEMENT] Distributed labor {} milliseconds after start.", start.elapsed().as_millis());
    l.produce_from_labor_manifest();
    println!("[SETTLEMENT] Successfully produced from buildings {} milliseconds after start.", start.elapsed().as_millis());

    let extra_turns = 25;
    for i in 0..extra_turns {
        l.refresh_job_list();
        l.distribute_labor();
        l.produce_from_labor_manifest();
    }
    println!("[SETTLEMENT] Repeated production cycle {} additional times {} milliseconds after start.", extra_turns, start.elapsed().as_millis());

    println!("[SETTLEMENT] Lestallum's storage is now: {:?}.", l.get_mut_stockpile());
    println!();
}

fn character_and_entity_test(world: &mut World, entity_list: &mut EntityList, location: (i32, i32, i32), start: Instant) {
    let mut protagonist = Character::new(String::from("Oyuunni Astraea"));
    let mut protag_entity = Entity::new_at(String::from("First Trailblazers"), String::from("FirstTrailblazers"), location.0, location.1, location.2);
    
    let (alias, index) = entity_list.add_entity(protag_entity);
    
    entity_list.get_mut_entity_with_alias(alias.clone()).add_unit(UnitGroup::Individual(protagonist));
    entity_list.get_entity_with_alias(alias.clone()).survey_here(world);

    let exploration_turns = 5;
    let mut rng = thread_rng();

    for turn in 0..exploration_turns {
        match rng.gen_range(0, 6) {
            0 => {
                match entity_list.move_aliased_entity(world, alias.clone(), Direction::East) {
                    Ok(loc) => {
                        println!("[ENTITY] {} moved east to {:?}.", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                        entity_list.get_entity_with_alias(alias.clone()).survey_here(world);
                    },
                    Err(loc) => {
                        println!("[ENTITY] {} tried to move east but couldn't and is still at {:?}", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                    },
                };
            },
            1 => {
                match entity_list.move_aliased_entity(world, alias.clone(), Direction::West) {
                    Ok(loc) => {
                        println!("[ENTITY] {} moved northeast to {:?}.", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                        entity_list.get_entity_with_alias(alias.clone()).survey_here(world);
                    },
                    Err(loc) => {
                        println!("[ENTITY] {} tried to move northeast but couldn't and is still at {:?}", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                    },
                };
            },
            2 => {
                match entity_list.move_aliased_entity(world, alias.clone(), Direction::Northwest) {
                    Ok(loc) => {
                        println!("[ENTITY] {} moved northwest to {:?}.", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                        entity_list.get_entity_with_alias(alias.clone()).survey_here(world);
                    },
                    Err(loc) => {
                        println!("[ENTITY] {} tried to move northwest but couldn't and is still at {:?}", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                    },
                };
            },
            3 => {
                match entity_list.move_aliased_entity(world, alias.clone(), Direction::Southeast) {
                    Ok(loc) => {
                        println!("[ENTITY] {} moved southeast to {:?}.", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                        entity_list.get_entity_with_alias(alias.clone()).survey_here(world);
                    },
                    Err(loc) => {
                        println!("[ENTITY] {} tried to move southeast but couldn't and is still at {:?}", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                    },
                };
            },
            4 => {
                match entity_list.move_aliased_entity(world, alias.clone(), Direction::Southwest) {
                    Ok(loc) => {
                        println!("[ENTITY] {} moved southwest to {:?}.", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                        entity_list.get_entity_with_alias(alias.clone()).survey_here(world);
                    },
                    Err(loc) => {
                        println!("[ENTITY] {} tried to move southwest but couldn't and is still at {:?}", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                    },
                };
            },
            5 => {
                match entity_list.move_aliased_entity(world, alias.clone(), Direction::Northeast) {
                    Ok(loc) => {
                        println!("[ENTITY] {} moved west to {:?}.", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                        entity_list.get_entity_with_alias(alias.clone()).survey_here(world);
                    },
                    Err(loc) => {
                        println!("[ENTITY] {} tried to move west but couldn't and is still at {:?}", entity_list.get_entity_with_alias(alias.clone()).name(), loc.as_tuple());
                    },
                };
            },
            _ => {
                println!("[ENTITY] Rand generated something weird...")
            }
        }
    }
    println!("{} travelled {} tiles over {} turns.", 
    entity_list.get_entity_with_alias(alias.clone()).name(), 
        world.get_tile_at(location)
            .unwrap()
            .distance_to(
                entity_list.get_entity_with_alias(alias.clone()).location().x(), 
                entity_list.get_entity_with_alias(alias.clone()).location().y(), 
                entity_list.get_entity_with_alias(alias.clone()).location().z()
            ),
        exploration_turns
    );
}