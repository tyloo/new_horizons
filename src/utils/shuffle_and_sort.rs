use std::collections::VecDeque;

use rand::*;

// Shuffles a VecDeque.
pub fn shuffle(vec: &mut VecDeque<(i32, i32, i32)>) {
    for n in 0..vec.len() {
        let mut rng = thread_rng();
        let a: u32 = rng.gen_range(0, vec.len() as u32);
        vec.swap(n as usize, a as usize);
    }
    for n in 0..vec.len() {
        let mut rng = thread_rng();
        let a: u32 = rng.gen_range(0, vec.len() as u32);
        let b: u32 = rng.gen_range(0, vec.len() as u32);
        vec.swap(a as usize, b as usize);
    }
}